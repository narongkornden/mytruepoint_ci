#!/bin/sh

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

echo $DIR

docker rm -f rpp_truepoint_api
docker rm -f rpp_truepoint_mongo
docker-compose rm

docker-compose build rpp_truepoint_api
WEB_ID=$(docker-compose up -d rpp_truepoint_api)

docker-compose build rpp_truepoint_mongo
DB_ID=$(docker-compose up -d rpp_truepoint_mongo)

sleep 3

docker exec -it rpp_truepoint_api sh /start_script.sh
docker exec -it rpp_truepoint_mongo sh /start_script.sh

sleep 3

docker exec -it rpp_truepoint_api composer install

