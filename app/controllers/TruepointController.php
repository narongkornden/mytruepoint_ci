<?php
namespace App\Controllers;

use App\Controllers\ApiController;

class TruepointController extends ApiController
{
    //------- start : Define variable ----------//
    public $service           = 'truepoint';
    private $getBurnpointRule = [
        [
            'type'   => 'required',
            'fields' => ['customer_id', 'content_type', 'content_id', 'point'],
        ],
    ];

    //------- start : Define variable ----------//
    private $getHistoryDetail = [
        [
            'type'   => 'required',
            'fields' => ['id'],
        ],
    ];

    private $validate = [
        'burn'     => [
            'redeem_by_campaign' => [
                [
                    'type'   => 'required',
                    'fields' => ['brand_id', 'outlet_id', 'terminal_id', 'tx_ref_id', 'acc_type', 'acc_value', 'campaign_code'],
                ],
            ],
            'redeem_by_code'     => [
                [
                    'type'   => 'required',
                    'fields' => ['brand_id', 'outlet_id', 'terminal_id', 'tx_ref_id', 'campaign_code', 'reward_code'],
                ],
            ],
            'apply_campaign'     => [
                [
                    'type'   => 'required',
                    'fields' => ['accesstoken', 'acc_type', 'acc_value', 'campaign_code', 'ssoid', 'os', 'quantity'],
                ],
            ],
            'reimburse'     => [
                [
                    'type'   => 'required',
                    'fields' => ['status', 'message','tmn_success_datetime','ref_transaction_id'],
                ],
            ],
        ],
        'rollback' => [
            'redeem_by_campaign' => [
                [
                    'type'   => 'required',
                    'fields' => ['brand_id', 'outlet_id', 'terminal_id', 'tx_ref_id'],
                ],
            ],
            'apply_campaign'     => [
                [
                    'type'   => 'required',
                    'fields' => ['ext_ref_id'],
                ],
            ],
        ],
    ];

    //------- end : Define variable --------------//

    //------- start: protected method ------------//
    protected function getTruepointRepository()
    {
        return $this->repository->getRepository('TruepointRepository');
    }

    //------- end: protected method ------------//

    //------- start: Main method ---------------//

    public function postBurnPointAction()
    {
        $inputs  = $this->getPostInput();
        $default = [];
        
        // Validate input
        if (!isset($this->validate['burn'][$inputs['action']])) {
            return $this->validateBussinessError('actionNotfound');
        }

        $params = $this->validateApi($this->validate['burn'][$inputs['action']], $default, $inputs);

        if (isset($params['msgError'])) {
            //Validate error
            return $this->validateError($params['fieldError'], $params['msgError']);
        }

        $truepointRepo = $this->getTruepointRepository();

        $result = $truepointRepo->burnPoint($params);
        if (!$result['success']) {
            return $this->validateBussinessError($result['message']);
        }

        //clear cache witch prefix
        //$this->cacheService->deleteCacheByPrefix($this->service);

        return $this->output($result, $inputs);
    }

    public function putUpdateReimburseAction()
    {
        $inputs  = $this->getPostInput();
        
        $default = [];

        if (!isset($this->validate['burn']['reimburse'])) {
            return $this->validateBussinessError('actionNotfound');
        }

        $params = $this->validateApi($this->validate['burn']['reimburse'], $default, $inputs);

        if (isset($params['msgError'])) {
            //Validate error
            return $this->validateError($params['fieldError'], $params['msgError']);
        }

        $truepointRepo = $this->getTruepointRepository();
        
        $result = $truepointRepo->updateReimburseStatus($params);
        
        if (!$result['success']) {
            
            if($result['message'] == 'statusNotfound'){
                return $this->validateBussinessError($result['message']);
            }

            elseif($result['message'] == 'statusNotchange'){
                return $this->validateBussinessError($result['message']);
            }

            else{
                return $this->validateBussinessError('updateFail');
            }
            
        }
        
        return $this->output($result);
    }

    public function postRollbackAction()
    {

        $inputs = $this->getPostInput();

        $default = [];
        //print_r($inputs['action']);exit;
        // Validate input
        if (!isset($this->validate['rollback'][$inputs['action']])) {
            return $this->validateBussinessError('actionNotfound');
        }

        $params = $this->validateApi($this->validate['rollback'][$inputs['action']], $default, $inputs);

        if (isset($params['msgError'])) {
            //Validate error
            return $this->validateError($params['fieldError'], $params['msgError']);
        }

        $truepointRepo = $this->getTruepointRepository();

        $result = $truepointRepo->rollback($params);

        //Check response error
        if (!$result['success']) {
            return $this->validateBussinessError($result['message']);
        }

        //clear cache witch prefix
        //$this->cacheService->deleteCacheByPrefix($this->service);

        return $this->output($result, $inputs);
    }

    public function getSearchByHistoryAction()
    {

        $inputs = $this->getAllUrlParam();

        //mange cache
        // $cacheData  = $this->getDataFromCache(__METHOD__, $inputs);

        // if (!empty($cacheData)) {

        //     return $this->output($cacheData, $inputs);

        // }

        $truepointRepo = $this->getTruepointRepository();
        //get tag data by input
        $result = $truepointRepo->getTransactionHistory($inputs);

        //Check response error
        if (!$result['success']) {
            return $this->validateBussinessError($result['message']);
        }

        if (isset($result['totalRecord'])) {
            $inputs['totalRecord'] = $result['totalRecord'];
        }

        return $this->output($result, $inputs);
    }

    public function countSearchByHistoryAction()
    {

        $inputs = $this->getAllUrlParam();

        //mange cache
        // $cacheData  = $this->getDataFromCache(__METHOD__, $inputs);

        // if (!empty($cacheData)) {

        //     return $this->output($cacheData, $inputs);

        // }

        $truepointRepo = $this->getTruepointRepository();
        //get tag data by input
        $result = $truepointRepo->countTransactionHistory($inputs);

        //Check response error
        if (!$result['success']) {
            return $this->validateBussinessError($result['message']);
        }

        if (isset($result['totalRecord'])) {
            $inputs['totalRecord'] = $result['totalRecord'];
        }

        return $this->output($result, $inputs);
    }

    //------- ebd: Main method ---------------//
}
