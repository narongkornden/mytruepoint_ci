<?php
use Phalcon\DI;
use Phalcon\DI\InjectionAwareInterface;
use Phalcon\Cli\Task;
use Phalcon\DiInterface;
use App\Repositories\TruepointRepository;

class BurnRetryTask extends Task implements InjectionAwareInterface
{
    public $di;

    //Method for create calcuateReposritory class
    private function getTruePointRepo()
    {
        return new TruepointRepository();
    }


    public function runAction(){
        $repo = $this->getTruePointRepo();
        $data = $repo->getListBurnretry();

        if (is_array($data)) {
            foreach ($data as $key => $value) {
                $burn = $repo->burnPointRetry($value);
            }
        }

        echo "total record = ".count($data)." ";
    }


}