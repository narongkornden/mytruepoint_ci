<?php
namespace App\Services;

use App\Services\Service;

class CampaigntrueyouService extends Service {

    private $serviceName = 'applycampaign';
    private $channel     = 'RPP';

    public function redeem($data,$params,$configKey=[]){

        $outputs = [ 
            'success' => true,
            'message' => '',
            'data'    => [],
        ];

        if ($configKey['method'] == 'rollback') {
            foreach ($data as $key => $value) {
                if (isset($value['success']) && ($value['success'] == true)) {
                    $param = $this->prepairRequestParams($value);
                    $res   = $this->rollback($param);
                
                    if (!$res['success']) {
                        $outputs['success'] = false;
                    }else{
                        $data[$key] = $res['data'];
                    }

                }
                
                unset($data['id']);
            }

            $outputs['data'] = $data;
        
        }else{
            $response = [];
            $quantity = (int)$params['quantity'];
            $quantity = ($quantity<=0)?1:$quantity;

            for ($i=1; $i <= $quantity; $i++) { 
                $res        = $this->applyCampaign($data);
                $response[] = $res;

                if (!$res['success']) {
                    $outputs['success']                   = false;
                    break;
                }

            }

            $outputs['data']['response']       = $response;
            $outputs['data']['code']           = $res['data']['code'];
            $outputs['data']['description']    = $res['data']['description'];
            $outputs['data']['transaction_id'] = $params['id'];

        }

        return $outputs;//

    }

    public function applyCampaign($params){

         //get config
        $config = $this->getServiceConfig($this->serviceName);

        //set base url
        $this->curl->header->set('Content-Type', 'application/xml');
        $this->curl->setBaseUri($config["redeem"]);

        //curl
        $response = $this->curl->post('', $params);

        $output = $this->convertData($response->body);

        $this->logService->writeResponseLog($this->serviceName, $config["redeem"], "POST", $params, json_encode($output));
        return $output;
    }


    public function rollback($params){
        //get config
        $config = $this->getServiceConfig($this->serviceName);

        //set base url
        $this->curl->setBaseUri($config["rollback"]);

        //curl
        $response = $this->curl->get('', $params);

        $output = $this->convertData($response->body);

        $this->logService->writeResponseLog($this->serviceName, $config["rollback"], "GET", $params, json_encode($output));

        return $output;

    }


    protected function prepairRequestParams($params){

        $data['message_id']  = $params['data']['transaction_id'];
        $data['channel']     = $this->channel;
        $data['description'] = 'rollback point';
        return $data;
    }


    protected function convertData($data)
    {

        $outputs = [ 
            'success' => true,
            'message' => '',
            'data'    => [],
        ];
        
        $xml   = simplexml_load_string($data, 'SimpleXMLElement', LIBXML_NOCDATA);
        $json  = json_encode($xml);
        $array = json_decode($json,TRUE);
        $array = $this->formatKeyResponse($array);
        
        if (isset($array['code'])) {

            if ($array['code'] == '0') {
                $array['code'] = 200;
            }

            if ($array['code'] == '200') {

                $outputs = [ 
                    'success' => true,
                    'message' => $array['description'],
                    'data'    => $array,
                ];

            }else{

                $outputs = [ 
                    'success' => false,
                    'message' => $array['description'],
                    'data'    => $array,
                ];
            }
            
        }

        return $outputs;

    }


    protected function formatKeyResponse($data){

        $formatKey = [
            'ErrorCode' => 'code',
            'ErrorDesc' => 'description'

        ];

        foreach ($data as $key => $value) {
            if (isset($formatKey[$key])) {
                $data[$formatKey[$key]] = $value;
                unset($data[$key]);
            }
        }

        if (isset($data['Data']['Ref_Trans'])) {
            $data['transaction_id'] = $data['Data']['Ref_Trans'];
        }
        
        return $data;

    }



}