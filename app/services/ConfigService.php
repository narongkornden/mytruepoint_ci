<?php
namespace App\Services;

use App\Services\Service;

class ConfigService extends Service {

    private $serviceName = 'config_link';

    public function getConfig($params) {
        $config = $this->getServiceConfig($this->serviceName);
        $this->curl->setBaseUri($config["url"]);

        $response = $this->curl->get($config['config'], $params);
        
        return $this->manageResponse($response, $this->serviceName);
    }

}