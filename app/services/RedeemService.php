<?php
namespace App\Services;

use App\Services\Service;

class RedeemService extends Service
{
    function __construct()
    {
         $this->logService = $this->service->getService("LogService");
    }

    private $serviceName = 'redeem';

    public function redeem($data, $params, $configKey = [])
    {
        $config = $this->getServiceConfig($this->serviceName);

        $this->curl->header->set('Content-Type', 'application/xml');
        $this->curl->setBaseUri($config["url"]);

        $response = $this->curl->post('', $data);
        $output   = $this->convertData($response->body);

        $message = $this->message->logmessage->redeem_customer;
        $additional = ["request"=>$data,"response"=>$output,"relation_id"=>$params['tx_ref_id'],"endpoint"=>$config["url"]];

        $graylog = $this->logService->graylog($message,$additional,5,null,null,null,null);

        $this->logService->writeResponseLog($this->serviceName, $config["url"], "POST", $data, json_encode($output));

        return $output;
    }

    public function redeem_dmp($params)
    {
        
        if($params['acc_type'] == 'CARD' && $params['campaign_type'] == 'MERCHANT'){
            $params['acc_type'] = 'TRUECARD';
        }
        
        $req_redeem = [
            "request" => [
                "trans_id" => date("ymdHis").rand(100,999),
                "verify_type" => strtolower($params['acc_type']),
                "verify_value" => $params['acc_value'],
                "campaign_code" => $params['campaign_code']
            ]
        ];
        $data = json_encode($req_redeem);
        
        $config = $this->getServiceConfig($this->serviceName);

        $this->curl->header->set('Content-Type', 'application/json');
        $this->curl->header->set('Authorization', 'Bearer ' . $config['tokenEarn']);

        $this->curl->setBaseUri($config["urlApply"]);
        $response = $this->curl->post('', $data);
        $output   = $this->parseJsonOutput($response->body, $params);

        $resSMS = $this->sms($params["mobile"], $output["message"]);

        $message = $this->message->logmessage->redeem_merchant;
        $additional = ["request"=>$req_redeem,"response"=>$output,"relation_id"=>$params['tx_ref_id'],"endpoint"=>$config["urlApply"]];
        $graylog = $this->logService->graylog($message,$additional,5,null,null,null,null);

        $this->logService->writeResponseLog($this->serviceName, $config["urlApply"], "POST", $data, json_encode($output));

        return $output;
    }

    public function verify($params)
    {


        $data = '<?xml version="1.0" encoding="UTF-8"?>
            <request>
                <method>verify_campaign</method>
                <reward_code>' . $params['reward_code'] . '</reward_code>
                <campaign_code>' . $params['campaign_code'] . '</campaign_code>
            </request>';

        $config = $this->getServiceConfig($this->serviceName);
        $this->curl->header->set('Content-Type', 'application/xml');
        $this->curl->header->set('Authorization', 'Bearer ' . $config['token']);
        $this->curl->setBaseUri($config["urlVerify"]);
        
        $this->logService->writeResponseLog($this->serviceName, $config["urlVerify"], "POST", $data, "");

        $response = $this->curl->post('', $data);

        $output = $this->convertData($response->body);

        $message = $this->message->logmessage->redeem_verify;

        $additional = ["request"=>$data,"response"=>$output,"endpoint"=>$config["urlVerify"]];

        $graylog = $this->logService->graylog($message,$additional,5,null,null,null,null);

        $this->logService->writeResponseLog($this->serviceName, $config["urlVerify"], "POST", $data, json_encode($response->body));

        return $output;
    }

    public function earn($params)
    {
        $data = [
            'transaction_id' => $params['transaction_id'],
            'channel'        => $params['channel'],
            'action'         => $params['action'],
            'msisdn'         => $params['msisdn'],
            'thaiid'         => $params['thaiid'],
            'multiplier'     => $params['multiplier'],
            'activity_date'  => date('Y-m-d H:i:s'),
            'ref1'           => $params['ref1'],
            'ref2'           => $params['ref2'],
            'ref3'           => $params['ref3'],
            'ref4'           => $params['ref4'],
            'ref5'           => $params['ref5'],
            'note'           => $params['note'],
        ];

        $config = $this->getServiceConfig($this->serviceName);
        $this->curl->header->set('Content-Type', 'application/json');
        $this->curl->header->set('Authorization', 'Bearer ' . $config['tokenEarn']);
        $this->curl->setBaseUri($config["urlEarn"]);

        $response = $this->curl->post('', json_encode($data));
        $output   = json_decode($response->body, 1);

        $message = $this->message->logmessage->redeem_earn;

        $additional = ["request"=>$data,"response"=>$response,"endpoint"=>$config["urlEarn"]];

        $graylog = $this->logService->graylog($message,$additional,5,null,null,null,null);

        $this->logService->writeResponseLog($this->serviceName, $config["urlEarn"], "POST", json_encode($data), json_encode($output));

        return $output;
    }

    public function sms($mobile="", $msg="")
    {
        // $mobile = "";
        $data = '<?xml version="1.0" encoding="utf-8"?>
        <request>
            <method>send_sms_by_other</method>
            <msisdn>'.$mobile.'</msisdn>
            <message>'.$msg.'</message>
        </request>';

        $config = $this->getServiceConfig($this->serviceName);
        $this->curl->header->set('Content-Type', 'application/xml');
        $this->curl->header->set('Authorization', 'Bearer ' . $config['tokenEarn']);
        $this->curl->setBaseUri($config["urlSMS"]);
        $response = $this->curl->post('', $data);
        $output   = $this->convertData($response->body);

        $this->logService->writeResponseLog($this->serviceName, $config["urlSMS"], "POST", $data, json_encode($output));
        return $output;
    }
    
    public function chkAcceptBrand($params)
    {
        $result    = false;
       
        $inputs = [
            'content_type' => 'saleapp',
            'content_id'   => 'merchant',
            'key'          => 'acceptBrand'
        ];

        $brandList = $this->configService->getConfig($inputs);
        
        if ((isset($brandList)) && ($brandList['success'] == true)) {

            $brandList = $brandList['data'][0]['configs'];
            
            if (array_key_exists($params['brand_id'], $brandList)) {
                $brandData = $brandList[$params['brand_id']];
                if (array_key_exists($params['campaign_code'], $brandData)) {
                    $campaignData = $brandData[$params['campaign_code']];
                    $result       = [
                        'code'    => $params['campaign_code'],
                        'channel' => $campaignData['channel'],
                        'action'  => $campaignData['action'],
                    ];
                }
            }   
        }
        
        return $result;
    }

    public function chkReimburseConfig($params)
    {
        
        $result    = [
            'status' => false
        ];
       
        $inputs = [
            'content_type' => 'saleapp',
            'content_id'   => 'merchant',
            'key'          => 'reimbursement'
        ];
        $imburseList = $this->configService->getConfig($inputs);
        
        if ((isset($imburseList)) && ($imburseList['success'] == true)) {

            $imburseList = $imburseList['data'][0]['configs'];
            
            if (array_key_exists($params['campaign_code'], $imburseList)) {
                $imburseData = $imburseList[$params['campaign_code']];
                
                $result       = [
                    'status' => true,
                    'code'    => $params['campaign_code'],
                    'imburse_amount' => $imburseData['imburse_amount']
                ];
            }   
        }
        
        return $result;
    }

    public function topupMerchant($params,$tx_ref_id)
    {
        $config = $this->getServiceConfig($this->serviceName);

        $this->curl->header->set('Content-Type', 'application/json');
        $this->curl->header->set('Accept', 'application/json');
        $this->curl->header->set('Authorization', 'Basic ' . $config['tokenTopup']);
        $this->curl->setBaseUri($config["urlTopup"]);
        
        $data = json_encode($params);
        
        $response = $this->curl->post('', $data);
        $output   = json_decode($response->body, 1);

        $message = $this->message->logmessage->reimburse_topup;
        $additional = ["request"=>$params,"response"=>$output,"relation_id"=>$tx_ref_id,"endpoint"=>$config["urlTopup"]];

        $graylog = $this->logService->graylog($message,$additional,2,null,null,null,null);
        
        $this->logService->writeResponseLog($this->serviceName, $config["urlTopup"], "POST", $data, json_encode($output));

        return $output;
    }

    protected function convertData($data)
    {
        $outputs = [
            'success' => true,
            'message' => '',
            'data'    => [],
        ];

        $xml   = simplexml_load_string($data, 'SimpleXMLElement', LIBXML_NOCDATA);
        $json  = json_encode($xml);
        $array = json_decode($json, true);

        if (isset($array['moreinfo'])) {
            $moreinfo = [];
            foreach ($array['moreinfo'] as $key => $value) {
                if (is_array($value) && empty($value)) {
                    $moreinfo[$key] = '';
                } else {
                    $moreinfo[$key] = $value;
                }
            }

            $array['moreinfo'] = $moreinfo;

            if (empty($array['moreinfo'])) {
                $array['moreinfo']['message'] = isset($array['description']) ? $array['description'] : '';
            }
        }

        if (isset($array['ext_transaction_id']) && empty($array['ext_transaction_id'])) {
            $array['ext_transaction_id'] = '';
        }

        if (isset($array['body']['items']['item'])) {
            if (!isset($array['body']['items']['item'][0])) {
                $array['body']['items'] = [$array['body']['items']['item']];
            } else {
                $array['body']['items'] = $array['body']['items']['item'];
            }
        }

        if ($array['code'] != 200) {
            if (!isset($array['description'])) {
                $array['description'] = $array['desc'];
            }

            $outputs = [
                'success' => false,
                'message' => $array['description'],
            ];
        }

        $outputs['data'] = $array;

        return $outputs;
    }

    protected function parseJsonOutput($json="", $params=[]) {
        $arr = json_decode($json, true);
        if(!is_array($arr)) {
            $outputs = [
                'success' => $arr['code']=="200"?"true":"false",
                'message' => $arr['message'],
                'data'    => null
            ];
        }else{
            $outputs = [
            'success' => $arr['code']=="200"?"true":"false",
            'message' => $arr['message'],
            'data'    => [
                "code" => @$arr['data']['source_code'],
                "description" => $arr['status'],
                "message" => $arr['message'],
                "service_desc" => "Truecard::request_benefit",
                "method" => "request_benefit",
                "channel" => "RPP",
                "campaign_type" => $params["campaign_type"],
                "mobile" => $params["mobile"],
                "point_balance" => @$arr['data']['point_balance'],
                "transaction_id" => $arr['data']['trans_id'],
                "ext_transaction_id" => $arr['data']['ext_transaction_id'],
                "moreinfo" => [
                    "promotion_code" => $this->getCodeFromMsg($arr['message']),
                    "message" => $arr['message'],
                    "point_remain" => @$arr['data']['point_balance'],
                    "point_action" => "N",
                    "point_redeem" => "0"
                ],
                "body" => [
                "items" => 
                [
                "0" => [
                    "campaign_code" => $arr['data']['response']['campaign_code'],
                    "campaign_name" => $arr['data']['response']['campaign_code'],
                    "benefit_total" => "0",
                    "benefit_use" => "0",
                    "benefit_balance" => "0"
                ]
                ]
                ]
            ]
            ];
        }
        
        return $outputs;
    }

    function getCodeFromMsg($msg="")
    {
        // preg_match_all('/([\d]+)/', $msg, $code);
        preg_match_all('/([0-9]{7,}+)/', $msg, $code);
        $reward_code = isset($code[0][0])?$code[0][0]:"";
        return $reward_code;
    }


}