<?php

namespace App\Services;

use App\Services\Service;

class LogService extends \Phalcon\Mvc\Micro {

    //private $serviceName = 'config';
    //------- start: Define variable ------//
    //------- end: Define variable ------//

    //------- start: Main function ------//
    //Method for manage folder, check if not exist create
    protected function manageFolder($service)
    {
        $path = str_replace('[service]', $service, $this->config->services->log->path);

        if (!file_exists($path)) {
            //create new
            mkdir($path, 0777, true);
        }

        return $path;
    }

    //Method for create file name
    protected function createFileName()
    {
        return date("Y-m-d-H").".txt";
    }

    //Method for create log data
    protected function createLogData($url, $method, $inputs, $response)
    {
        //Define output
        $datas = "";

        $datas .= "Timestamp : ".date("Y-m-d H:i:s")."\n";
        $datas .= "Url request : ".$url."\n";
        $datas .= "Method : ".$method."\n";
        $datas .= "Parameter : ".((is_array($inputs))?json_encode($inputs):$inputs)."\n";
        $datas .= "Response : \n";
        $datas .= $response."\n";
        $datas .= "==========================\n\n";

        return $datas;
    }

    //Method for write response log
    protected function writelog($fullpath, $url, $method, $inputs, $response)
    {
        $file = fopen($fullpath, 'a');

        //create log data
        $datas = $this->createLogData($url, $method, $inputs, $response);
        fwrite($file, $datas);
        fclose($file);
    }

    //Method for write log
    public function writeResponseLog($service, $url, $method, $inputs, $response) 
    {
        //get folder path in not exist create it
        $path     = $this->manageFolder($service);
        //create file name
        $filename = $this->createFileName();
        
        //write log
        $this->writelog($path.$filename, $url, $method, $inputs, $response);
    }

    //log service funtion
    public function graylog($shmessag,$additional = [],$level,$fullmes= null,$line = null,$file = null,$version= null)
    {
        //get config
        $config  = $this->config->services->graylog;

        $environment = getenv('ENVIRONMENT');

        $transport = new \Gelf\Transport\TcpTransport($config['tcp'], $config['port']);
        $publisher = new \Gelf\Publisher();

        $publisher->addTransport($transport);

        $message = new \Gelf\Message();

        $message->setShortMessage($shmessag)

        ->setFacility($environment)

        ->setHost($_SERVER['HTTP_HOST']);

        if (!empty($additional)) {
            foreach($additional as $key=>$value){
                $message->setAdditional($key,$value);
            }
        }

        if ($fullmes) {
            $message->setFullMessage($fullmes);
        }

        if ($line) {
            $message->setLine($line);
        }
        if ($file) {
            $message->setFile($file);
        }
        if ($version) {
            $message->setVersion($version);
        }

        switch ($level) {
            case 0:
                $message->setLevel(\Psr\Log\LogLevel::EMERGENCY);
                break;
            case 1:
                $message->setLevel(\Psr\Log\LogLevel::ALERT);
                break;
            case 2:
                $message->setLevel(\Psr\Log\LogLevel::CRITICAL);
                break;
            case 3:
                $message->setLevel(\Psr\Log\LogLevel::ERROR);
                break;
            case 4:
                $message->setLevel(\Psr\Log\LogLevel::WARNING);
                break;
            case 5:
                $message->setLevel(\Psr\Log\LogLevel::NOTICE);
                break;
            case 6:
                $message->setLevel(\Psr\Log\LogLevel::INFO);
                break;
            case 7:
                $message->setLevel(\Psr\Log\LogLevel::DEBUG);
        }

        try{
             $publisher->publish($message);
             $result = true;
        }
        catch (\Exception $e) {
            $result = false;
        }


        return  $result;
    }
    //------- end: Main function ------//
}
