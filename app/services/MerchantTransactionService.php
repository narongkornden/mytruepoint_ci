<?php
namespace App\Services;

use App\Services\Service;

class MerchantTransactionService extends Service {

    private $serviceName = 'merchanttransaction';

    public function getMerchant($params)
    {
       
        $param = [
            'trueyou.merchant_id' => $params['brand_id'].$params['outlet_id']
        ];
        //get config
        $config = $this->getServiceConfig($this->serviceName);
        //set base url
        $this->curl->setBaseUri($config["url"]);
        $response = $this->curl->get($config['merchant'],$param);
        return $this->manageResponse($response, $this->serviceName);

    }

    public function getMerchantDetail($params)
    {
        //get config
        $config = $this->getServiceConfig($this->serviceName);
       
        $param = [
            'id' => $params
        ];
        //set base url
        $this->curl->setBaseUri($config["url"]);
        $response = $this->curl->get($config['merchantdetail'], $param);
        return $this->manageResponse($response, $this->serviceName);

    }

}
