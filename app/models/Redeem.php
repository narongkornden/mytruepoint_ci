<?php

namespace App\Models;

use App\Models\Models;

class Redeem extends Models
{
    
    public $tx_ref_id;
    public $brand_id;
    public $outlet_id;
    public $terminal_id;
    public $account_type;
    public $account_value;
    public $campaign_code;

    // for burn by apply campaign
    public $accesstoken;
    public $ssoid;
    public $os;

    public $reward_code;
    public $status;
    public $action;
    public $channel;
    public $ext_response_date;
    public $ext_response_data;
    public $ext_ref_id;
    public $created_date;
    public $updated_date;

    public function initialize()
    {
        $this->setSource('redeem');
    }
    
    public function getSource()
    {
        return 'redeem';
    }
}