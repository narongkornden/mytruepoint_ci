<?php

namespace App\Models;

use App\Models\Models;

class History extends Models
{
    
    public function initialize()
    {
        $this->setSource('history');
    }
    
    public function getSource()
    {
        return 'history';
    }
}