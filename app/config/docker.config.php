<?php

$settings = [
    // 'database' => [
    //     'mongo' => [
    //         'host'     => 'rpp_truepoint_mongo',
    //         'port'     => '27017',
    //         'username' => '',
    //         'password' => '',
    //         'dbname'   => 'rpp_truepoint',
    //     ],
    // ],
    'database'    => [
        'mongo' => [
            'host'     => '192.168.120.69',
            'port'     => '27017',
            'username' => 'rpp_truepoint',
            'password' => '1qaz2wsx',
            'dbname'   => 'rpp_truepoint',
        ],
    ],
    'application' => [
        'repoDir'        => __DIR__ . '/../../app/repositories/',
        'servicesDir'    => __DIR__ . '/../../app/services/',
        'viewsDir'       => __DIR__ . '/../../app/views/',
        'modelsDir'      => __DIR__ . '/../../app/models/',
        'controllersDir' => __DIR__ . '/../../app/controllers/',
        'libraryDir'     => __DIR__ . '/../../app/library/',
        'baseUri'        => 'http://rpp-truepoint-ms-api.dev',
    ],
    'curl_api'    => [
        'truepoint' => '',
    ],
    'services'   => [
        'redeem'     => [
            'url'        => 'https://am-rpp-alpha.eggdigital.com/truecardbn/v1/truecardsrv/services/partner.aspx',
            'urlVerify'  => 'http://dmpapi-dev.trueid.net/trueyoucore-servicecentral/',
            'urlApply'   => 'https://dmpapi2.trueid-dev.net/trueyoucore-tyredeemprivilege/',
            'urlEarn'    => 'https://dmpapi2.trueid-dev.net/eventrd-producer/v2/campaign/tycore/TYMerchantTYCEarnPoint',
            'urlSMS'     => 'https://dmpapi2.trueid-dev.net/trueyoucore-servicecentral/',
            'token'      => '59351c4b233eaa4928abdc007d644070304543708163cdfcc0174c69',
            'tokenEarn'  => '5a9d0732a0e62103bcf9e483a1795d0c18364ba7a2e24f12222d115a',
            'urlTopup'   => 'https://am-rpp-alpha.eggdigital.com/campaign-api-service/v1/api/topup',
            'tokenTopup' => 'aW50ZXJuYWw6aW50ZXJuYWw=',
        ],
        'merchanttransaction' => [
            'url'                => 'https://am-rpp-alpha.eggdigital.com/crm-ms-mtransaction-api/v1/',
            'merchant'           => 'merchant',
            'merchantdetail'     => 'merchant/detail',
        ],
        'log'           => [
            'path' => '/data/logs/api/[service]/',
        ],
        'applycampaign' => [
            'redeem'   => 'https://am-rpp-alpha.eggdigital.com/trueyou-point-applycampaign/v1/',
            'rollback' => 'https://am-rpp-alpha.eggdigital.com/trueyou-point-rollback/v1/',
        ],
        'config_link'  => [
            'url'     => 'https://am-rpp-alpha.eggdigital.com/crm-ms-config-api/v1/',
            'config'    => 'config/detail',    
        ],
        'graylog'  => [
            'tcp'     => 'elog.eggdigital.com',
            'port'    => 12201,    
         ],
    ],
    

    'cache'       => [
        'configs'  => [
            'cacheDir' => '../app/cache/',
        ],
        'lifeTime' => 1800,
    ],
];
