<?php

use Phalcon\Mvc\Router\Group as RouterGroup;

$router->removeExtraSlashes(true);

$router->setDefaults(array(
    'namespace'  => 'App\Controllers',
    'controller' => 'error',
    'action'     => 'page404'
));

//==========Route for api==========
$api = new RouterGroup(array(
    'namespace' => 'App\Controllers'
));


$api->addPost('/burn', [
    'controller' => 'truepoint',
    'action'     => 'postBurnPoint',
]);

$api->addPut('/reimburse', [
    'controller' => 'truepoint',
    'action'     => 'putUpdateReimburse',
]);

$api->addPost('/rollback', [
    'controller' => 'truepoint',
    'action'     => 'postRollback',
]);

$api->addGet('/redeems/search', [
    'controller' => 'truepoint',
    'action'     => 'getSearchByHistory',
]);


$api->addGet('/redeems/count', [
    'controller' => 'truepoint',
    'action'     => 'countSearchByHistory',
]);
// $api->addPost('/earn', [
//     'controller' => 'truepoint',
//     'action'     => 'postEarnPoint',
// ]);

// $api->addGet('/history', [
//     'controller' => 'truepoint',
//     'action'     => 'getSearchHistory',
// ]);

// $api->addGet('/history/detail', [
//     'controller' => 'truepoint',
//     'action'     => 'getHistoryDetail',
// ]);

//==========Redeem API =========

// $api->addPost('/redeems/campaign', [
//     'controller' => 'redeem',
//     'action'     => 'postRedeemByCampaign',
// ]);

// $api->addPost('/redeems/code', [
//     'controller' => 'redeem',
//     'action'     => 'postRedeemByCode',
// ]);

// $api->addPost('/redeems/reversal', [
//     'controller' => 'redeem',
//     'action'     => 'postRedeemReversal',
// ]);

// $api->addGet('/redeems/search', [
//     'controller' => 'truepoint',
//     'action'     => 'getSearchByHistory',
// ]);

// $api->addGet('/customers/cards', [
//     'controller' => 'redeem',
//     'action'     => 'getCustomerCard',
// ]);

$router->mount($api);

return $router;
