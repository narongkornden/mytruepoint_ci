<?php

$loader = new \Phalcon\Loader();

$loader->registerDirs(
    [
        $config->application->servicesDir,
        $config->application->libraryDir,
        $config->application->repoDir,
        $config->application->viewsDir,
        $config->application->modelsDir,
        $config->application->controllersDir,
    ]
);

$loader->registerNamespaces(array(
    'App\\Controllers'  => $config->application->controllersDir,
    'App\\Repositories' => $config->application->repoDir,
    'App\\Services'     => $config->application->servicesDir,
    'App\\Library'      => $config->application->libraryDir,
    'App\\Model'        => $config->application->modelsDir,
    'Gelf\\Transport'   => 'vendor/graylog2/gelf-php/src/Gelf/Transport',
    'Psr\\Log'          => 'vendor/psr/log/Psr/Log',
    'Gelf'              => 'vendor/graylog2/gelf-php/src/Gelf',
));


$loader->register();

include __DIR__.'/../../vendor/autoload.php';

