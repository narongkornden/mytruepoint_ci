<?php
return new \Phalcon\Config( [

    /* Mission Fail */
    'missionFail' => [
        'code'     => 400,
        'msgError' => 'Mission Fail',
    ],

    'redeemFail' => [
        'code'     => 400,
        'msgError' => 'Redeem Fail',
    ],

    'verifyFail' => [
        'code'     => 400,
        'msgError' => 'Verify Fail',
    ],

    'updateFail' => [
        'code'     => 400,
        'msgError' => 'Update Fail',
    ],

    /* Data Not Found */
    'dataNotFound' => [
        'code'     => 400,
        'msgError' => 'Data Not Found',
    ],

    /* Cannot Connect to Database */
    'connectDBError' => [
        'code'     => 400,
        'msgError' => 'Cannot Connect to Database',
    ],

    /* Insert Error */
    'insertError' => [
        'code'     => 400,
        'msgError' => 'Insert Error',
    ],

    /* Update Error */
    'updateError' => [
        'code'     => 400,
        'msgError' => 'Update Error',
    ],

    /* Delete Error */
    'deleteError' => [
        'code'     => 400,
        'msgError' => 'Delete Error',
    ],


    /* Data is duplicate */
    'dataDuplicate' => [
        'code'     => 400,
        'msgError' => 'Data is duplicate',
    ],

    /* type not found */
    'typeNotFound' => [
        'code'     => 400,
        'msgError' => 'Type not found',
    ],

    /* burn error */
    'burnError' => [
        'code'     => 400,
        'msgError' => 'Cannot burn point',
    ],

    /* earn error */
    'earnError' => [
        'code'     => 400,
        'msgError' => 'Cannot earn point',
    ],

    /* redeem error */
    'redeemError' => [
        'code'     => 400,
        'msgError' => 'Cannot redeem transaction',
    ],

    /* rollback error */
    'rollbackError' => [
        'code'     => 400,
        'msgError' => 'Cannot roll back transaction',
    ],

    'actionNotfound' => [
        'code'     => 400,
        'msgError' => 'Action Not found',
    ],

    'channelNotfound' => [
        'code'     => 400,
        'msgError' => 'Channel Not found',
    ],

    'statusNotfound' => [
        'code'     => 400,
        'msgError' => 'Status Not found',
    ],
    'statusNotchange' => [
        'code'     => 400,
        'msgError' => 'Cannot change success status',
    ],
    'logmessage' => [
        'redeem_insert'           => 'insert data to rpp_truepoint collection redeem',
        'rollback_update'         => 'update transaction on rpp_truepoint status rollback',
        'reimburse_update'        => 'update redeem status to success for reimburseMerchant',
        'redeem_customer'         => 'trueyou redeem for customer',
        'redeem_merchant'         => 'trueyou redeem for merchant',
        'reimburse_topup'         => 'top up merchant',
        'redeem_verify'           => 'verify campaign_code or reward_code',
        'redeem_earn'             => 'customer earn point',
    ],


] );