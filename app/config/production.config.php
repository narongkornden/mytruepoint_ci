<?php

$settings = [
    'database'    => [
        'mongo' => [
            'host'     => '192.168.55.150',
            'port'     => '27017',
            'username' => 'rpp_truepoint',
            'password' => 'ZQ5781#vf8483',
            'dbname'   => 'rpp_truepoint',
        ],
    ],
    'application' => [
        'repoDir'        => __DIR__ . '/../../app/repositories/',
        'servicesDir'    => __DIR__ . '/../../app/services/',
        'viewsDir'       => __DIR__ . '/../../app/views/',
        'modelsDir'      => __DIR__ . '/../../app/models/',
        'controllersDir' => __DIR__ . '/../../app/controllers/',
        'libraryDir'     => __DIR__ . '/../../app/library/',
        'baseUri'        => 'https://am-rpp.eggdigital.com/crm-ms-truepoint-api/v1/',
    ],
    'curl_api'    => [
        'truepoint' => '',
    ],
    'services'    => [
        'redeem'        => [
            'url'         => 'https://am-rpp.eggdigital.com/truecardbn/v1/truecardsrv/services/partner.aspx',
            'urlVerify'   => 'https://dmpapi2.trueid.net/trueyoucore-servicecentral/',
            'urlApply'    => 'https://dmpapi2.trueid.net/trueyoucore-tyredeemprivilege/',
            'urlEarn'     => 'https://dmpapi2.trueid.net/eventrd-producer/v2/campaign/tycore/TYMerchantTYCEarnPoint',
            'urlSMS'      => 'https://dmpapi2.trueid.net/trueyoucore-servicecentral/',
            'token'       => '5aaf9ade15afe0324400bacca86d57914e4c4382b7d77762cc7d2ee3',
            'tokenEarn'   => '5aaf9ade15afe0324400bacca86d57914e4c4382b7d77762cc7d2ee3',
            'urlTopup'   => 'https://am-internal-rpp.eggdigital.com/campaign-api-service/v1/api/topup',
            'tokenTopup' => 'cnBwX3NlcnZpY2U6IEtvMXFTbXAyU2ox',
            
        ],
        'merchanttransaction' => [
            'url'                => 'https://am-rpp.eggdigital.com/crm-ms-mtransaction-api/v1/',
            'merchant'           => 'merchant',
            'merchantdetail'     => 'merchant/detail',
        ],
        'log'           => [
            'path' => '/data/logs/api/[service]/',
        ],
        'applycampaign' => [
            'redeem'   => 'https://am-rpp-alpha.eggdigital.com/trueyou-point-applycampaign/v1/',
            'rollback' => 'https://am-rpp-alpha.eggdigital.com/trueyou-point-rollback/v1/',
        ],
        'config_link' => [
            'url'         => 'https://am-rpp.eggdigital.com/crm-ms-config-api/v1/',
            'config'      => 'config/detail',
        ],
        'graylog'  => [
            'tcp'     => 'elog.eggdigital.com',
            'port'    => 12201,
        ],
    ],


    'cache'       => [
        'configs'  => [
            // 'cacheDir' => '../app/cache/',
            'host'       => '192.168.50.160',
            'port'       => 11211,
            'persistent' => false,
            'statsKey'   => '_PHCM',
        ],
        'lifeTime' => 1800,
    ],

];
