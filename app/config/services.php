<?php
use Phalcon\DI\FactoryDefault;
use Phalcon\Mvc\View;
use Phalcon\Mvc\Router;
use Phalcon\Http\Request;
use Phalcon\Http\Response;

use Phalcon\Mvc\Collection\Manager;
use Phalcon\Db\Adapter\MongoDB\Client;
use Phalcon\Http\Client\Request as Curl;
use Phalcon\Cache\Backend\File as BackFile;
use Phalcon\Cache\Frontend\Data as FrontData;

use Phalcon\Cache\Backend\Memcache;

date_default_timezone_set('asia/bangkok');

// Create a DI
$di = new FactoryDefault();

//Registering a router
$di->set('router', function ()
{
    $router = new Router();
    require 'routes.php';
    return $router;
});

// Setup the view component
$di->set(
    "view",
    function () use ($config) {
        $view = new View();
        $view->setViewsDir($config->application->viewsDir);
        return $view;
    }
);



$di->set('dispatcher', function(){
    // Create/Get an EventManager
    $eventsManager = new Phalcon\Events\Manager();

    // Attach a listener
    $eventsManager->attach("dispatch", function ($event, $dispatcher, $exception) {
        // The controller exists but the action not
        if ($event->getType() == 'beforeNotFoundAction') {
            $dispatcher->forward(array(
                'namespace' => 'App\Controllers',
                'controller' => 'error',
                'action' => 'page404'
            ));
            return false;
        }
        // Alternative way, controller or action doesn't exist
        if ($event->getType() == 'beforeException') {
            switch ($exception->getCode()) {
                case Phalcon\Dispatcher::EXCEPTION_HANDLER_NOT_FOUND:
                case Phalcon\Dispatcher::EXCEPTION_ACTION_NOT_FOUND:
                    $dispatcher->forward(array(
                        'namespace' => 'App\Controllers',
                        'controller' => 'error',
                        'action' => 'page404'
                    ));
                    return false;
            }
        }
    });

    $dispatcher = new Phalcon\Mvc\Dispatcher();

    // Bind the EventsManager to the dispatcher
    $dispatcher->setEventsManager($eventsManager);

    return $dispatcher;
});

// // Initialise the mongo DB connection.
// $di->setShared('mongo', function () {
//     /** @var \Phalcon\DiInterface $this */
//     $config = $this->getShared('config');

//     if (!$config->database->mongo->username || !$config->database->mongo->password) {
//         $dsn = 'mongodb://' . $config->database->mongo->host.":". $config->database->mongo->port;
//     } else {
//         $dsn = sprintf(
//             'mongodb://%s:%s@%s:%s/%s',
//             $config->database->mongo->username,
//             $config->database->mongo->password,
//             $config->database->mongo->host,
//             $config->database->mongo->port,
//             $config->database->mongo->dbname
//         );
//     }

//     $mongo = new Client($dsn);

//     return $mongo->selectDatabase($config->database->mongo->dbname);
// });

//Initialise the mongo DB connection (original).
$di->setShared('mongo', function () {
    $config = $this->getShared('config');

    if (!$config->database->mongo->username || !$config->database->mongo->password) {
        $dsn = 'mongodb://' . $config->database->mongo->host.":". $config->database->mongo->port;
    } else {
        $dsn = sprintf(
            'mongodb://%s:%s@%s:%s/%s',
            $config->database->mongo->username,
            $config->database->mongo->password,
            $config->database->mongo->host,
            $config->database->mongo->port,
            $config->database->mongo->dbname
        );
    }

    $mongo = new \MongoDB\Driver\Manager($dsn);

    return $mongo;
});

// Initialise the mongo DB connection (original).
$di->setShared('mongoOrig', function () {
    $config = $this->getShared('config');

    if (!$config->database->mongo->username || !$config->database->mongo->password) {
        $dsn = 'mongodb://' . $config->database->mongo->host.":". $config->database->mongo->port;
    } else {
        $dsn = sprintf(
            'mongodb://%s:%s@%s:%s/%s',
            $config->database->mongo->username,
            $config->database->mongo->password,
            $config->database->mongo->host,
            $config->database->mongo->port,
            $config->database->mongo->dbname
        );
    }

    $mongo = new \MongoDB\Driver\Manager($dsn);

    return $mongo;
});

$di->set('collectionManager', function () {
    return new Manager();
}, true);

// Register a "repository" service in the container
$di->set('repository', function () {
    $repository =  new App\Repositories\Repositories();
    return $repository;
});

// Register a "model" service in the container
$di->set('model', function () {
    $model =  new App\Models\Models();
    return $model;
});

// Register a "myLibrary" service in the container
$di->set('myLibrary', function () {
    $myLib =  new App\Library\MyLibrary();
    return $myLib;
});

// Register a "mongoService" service in the container
$di->set('mongoService', function () {
    $myLib =  new App\Services\MongoService();
    return $myLib;
});

// Register a "response" service in the container
$di->set('response', function () {
    $response = new Response();
    return $response;
});

// Register a "cacheService" service in the container
$di->set('cacheService', function () {
    $cacheService =  new App\Services\CacheService();
    return $cacheService;
});

// Register a "cacheService" service in the container
$di->set('cache', function () {
    $config = $this->getShared('config');

    $frontCache = new FrontData(
        [
            'lifetime' => $config->cache->lifeTime,
        ]
    );

    $cacheConfig = $config->cache->configs->toArray();

    if (isset($cacheConfig['cacheDir'])) {
        $cache = new BackFile(
            $frontCache,
            $cacheConfig
        );
    } else {
        $cache = new Memcache(
            $frontCache,
            $cacheConfig
        );
    }

    return $cache;
});


// Register a "request" service in the container
$di->set('request', function () {
    $request = new Request();
    return $request;
});

// Register a "curl" service in the container
$di->set('curl', function () {
    $curl = Curl::getProvider();
    return $curl;
});

// Register a "service" service in the container
$di->set('service', function () {
    $service =  new App\Services\Service();
    return $service;
});

// Register a "logService" service in the container
$di->set('logService', function () {
    $logService =  new App\Services\LogService();
    return $logService;
});

$di->set('configService', function () {
    $configService =  new App\Services\ConfigService();
    return $configService;
});

$di->set('transactionService', function () {
    $transactionService =  new App\Services\MerchantTransactionService();
    return $transactionService;
});

//add config and message
$di->set('config', $config, true);
$di->set('status', $status, true);
$di->set('message', $message, true);

