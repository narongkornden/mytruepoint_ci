<?php
namespace App\Repositories;

use Phalcon\Exception;

class Repositories extends \Phalcon\Mvc\Micro {

    public function getRepository($name)
    {
        $className = "\\App\\Repositories\\{$name}";

        if (!class_exists($className)) {
            throw new Exception("Model Class {$className} doesn't exists.");
        }

        return new $className();
    }

    protected function curl($url, $method, $params) {

        try {
            $client = new Client(
                [
                    'base_uri' => $url,
                    'verify' => false
                ]
            );
          
            $res    = $client->request($method, '', $params);
            $result = $res->getBody();

            return json_decode($result, true);

        } catch (\Exception $e) {
            exit('Process Not Working');
            $msg['0'] = 'Uh oh! ' . $e->getMessage();
            $msg['1'] = 'HTTP request URL: ' . $e->getRequest()->getUrl();
            $msg['2'] = 'HTTP request: ' . $e->getRequest();
            $msg['3'] = 'HTTP response status: ' . $e->getResponse()->getStatusCode();
            $msg['4'] = 'HTTP response: ' . $e->getResponse();

            return $error['error_curl_ms'] = $msg;
        }

    }
    
}