<?php
namespace App\Repositories;

use App\Models\Redeem;
use Phalcon\Mvc\Model\Transaction\Failed;

class TruepointRepository extends \Phalcon\Mvc\Micro
{
    //------- start : Define variable ----------//

        // Log level
        // EMERGENCY      0
        // ALERT,         1
        // CRITICAL,      2
        // ERROR,         3
        // WARNING,       4
        // NOTICE,        5
        // INFO,          6
        // DEBUG          7

    private $channel = 'RPP';
    private $defaultLimit = '500';
    private $defaultPage  = '1';

    protected $channelRollback = ['postman', 'bs', 'exchange', 'cronjob'];

    protected $statusType = [
        'new'           => 'new',
        'success'       => 'success',
        'waiting'       => 'waiting',
        'error'         => 'fail',
        'rollback'      => 'rollback',
        'rollback_fail' => 'rollback_fail',
    ];

    protected $actionList = ['REDEEM_BY_CAMPAIGN', 'REDEEM_BY_CODE', 'APPLY_CAMPAIGN'];

    protected $configKey = [
        'burn'     => [
            'redeem_by_campaign' => [
                'method'     => 'request_benefit',
                'action'     => 'REDEEM_BY_CAMPAIGN',
                'service'    => 'redeem',
                'allowFeild' => ['brand_id', 'outlet_id', 'terminal_id', 'tx_ref_id', 'acc_type', 'acc_value', 'campaign_code','campaign_type', 'mobile'],
            ],
            'redeem_by_code'     => [
                'method'     => 'mark_use',
                'action'     => 'REDEEM_BY_CODE',
                'service'    => 'redeem',
                'allowFeild' => ['brand_id', 'outlet_id', 'terminal_id', 'tx_ref_id', 'acc_type', 'acc_value', 'campaign_code', 'reward_code','campaign_type', 'mobile'],

            ],
            'apply_campaign'     => [
                'method'     => 'APPLYCAMPAIGN',
                'action'     => 'APPLY_CAMPAIGN',
                'service'    => 'campaign',
                'allowFeild' => ['accesstoken', 'acc_type', 'acc_value', 'campaign_code', 'ssoid', 'os', 'quantity'],
            ],
        ],
        'rollback' => [
            'redeem_by_campaign' => [
                'method'               => 'rollback',
                'action'               => 'REDEEM_BY_CAMPAIGN',
                'service'              => 'redeem',
                'allowFeild'           => ['brand_id', 'outlet_id', 'terminal_id', 'tx_ref_id', 'campaign_code', 'id'],
                'searchFeild'          => ['tx_ref_id', 'brand_id'],
                'requiedFeildRollback' => ['campaign_code', 'ext_ref_id', 'account_type', 'account_value', 'terminal_id', 'tx_ref_id', 'id', 'action'],
            ],
            'apply_campaign'     => [
                'method'               => 'rollback',
                'action'               => 'APPLY_CAMPAIGN',
                'service'              => 'campaign',
                'allowFeild'           => ['ext_ref_id'],
                'searchFeild'          => ['ext_ref_id'],
                'requiedFeildRollback' => ['ext_ref_id', 'id'],
            ],
        ],
    ];

    protected $configKeyAccType = [
        'CARD'    => 'cardid',
        'THAIID'  => 'thaiid',
        'MOBILE'  => 'productid',
        'default' => '',

    ];

    private $allowFilter = ['tx_ref_id', 'brand_id', 'outlet_id', 'terminal_id', 'account_type', 'account_value', 'campaign_code', 'reward_code', 'status', 'action', 'ext_response_date', 'updated_date', 'id', 'ext_ref_id', 'body', 'items','ext_response_data.reimburse.mobile','ext_response_data.reimburse.status'];

    private $mapFeild = [
        'terminal_id'     => 'terminal_id',
        'acc_type'        => 'verify_type',
        'acc_value'       => 'verify_value',
        'tx_ref_id'       => 'ext_transaction_id',
        'promotion_code'  => 'reward_code',
        'reason_rollback' => 'action',
        'transaction_id'  => 'transaction_id',
        'reward_code'     => 'promotion_code',
        'accesstoken'     => 'AccessToken',
        'THAIID'          => 'ThaiID',
        'CampaignCode'    => 'CampaignCode',
        'ssoid'           => 'SSOID',
        'os'              => 'OS',
        'ext_ref_id'      => 'transaction_id',
        'action'          => 'reason_rollback',

    ];

    private $insertKey = [
        'tx_ref_id'     => 'tx_ref_id',
        'brand_id'      => 'brand_id',
        'outlet_id'     => 'outlet_id',
        'terminal_id'   => 'terminal_id',
        'acc_type'      => 'account_type',
        'acc_value'     => 'account_value',
        'campaign_code' => 'campaign_code',
        'reward_code'   => 'reward_code',
        'accesstoken'   => 'accesstoken',
        'ssoid'         => 'ssoid',
        'os'            => 'os',

    ];

    //------- end : Define variable ----------//
    public function __construct()
    {
        parent::__construct();
    }

    //------- end : Define variable ----------//

    //------- start: protected method ------------//

    protected function getRedeemModel()
    {
        return $this->model->getModel('Redeem');
    }

    //Method for get services
    protected function getServiceClass($name)
    {

        $service = null;
        switch ($name) {
            case "redeem":
                $service = $this->service->getService("RedeemService");
                break;
            case "campaign":
                $service = $this->service->getService("CampaigntrueyouService");
                break;
            case "transaction":
                $service = $this->service->getService("MerchantTransactionService");
                break;
            case "log":
                $service = $this->service->getService("LogService");
                break;
        }

        return $service;
    }

    public function getDataById($id)
    {
        $redeemModel = $this->getRedeemModel();
        return $redeemModel->findById($id);
    }

    public function getDataByTransactionId($transaction_id)
    {
        $redeemModel = $this->getRedeemModel();
        return $redeemModel->findByTransactionId($transaction_id);
    }

    //Method for insert data to db
    protected function insertData($model, $params)
    {
        $service   = $this->getServiceClass('log');
        //add deal data to model
        foreach ($params as $key => $value) {
            if (property_exists($model, $key)) {
                $model->{$key} = $value;
            }
        }

        if (!$model->save()) {
            return null;
        }

        $response = $model->getOnlyData();

        $message = $this->message->logmessage->redeem_insert;
        $additional = ["request"=>$params,"response"=>$response,"relation_id"=>$params['tx_ref_id'],"endpoint"=>"/burn"];

        $graylog = $service->graylog($message,$additional,4,null,null,null,null);

        return $response;
    }

    //Method for update data to db
    public function updateData($model, $params)
    {

        $service   = $this->getServiceClass('log');

        foreach ($params as $key => $value) {
            if (property_exists($model, $key)) {
                $model->{$key} = $value;
            }
        }
    
        if (!$model->save()) {
            return null;
        }

        $response = $model->getOnlyData();

        if (isset($params['status']) && $params['status'] == 'rollback') {

                $additional = ["request"=>$params,"response"=>$response,"relation_id"=>$params['ext_ref_id'],"endpoint"=>"/rollback"];

                $message = $this->message->logmessage->rollback_update;

                $graylog = $service->graylog($message,$additional,5,null,null,null,null);
        }
       
        return $response;
    }

    public function updateStatus($model, $params)
    {
        $service = $this->getServiceClass('log');
        $allowStatus = ['SUCCESS','FAIL'];
        
        $oldData = $model->{'ext_response_data'}->{'reimburse'}->{'status'};

        $result = [
            'success'    => false,
            'message'   => ''
        ]; 

        if(isset($oldData) && $oldData == 'SUCCESS'){
            
            $result = [
                'success'    => false,
                'message'   => 'statusNotchange' 
            ]; 
            
            return $result;
        }

        if (!in_array($params['status'],$allowStatus)) {
            
            $result = [
                'success'    => false,
                'message'   => 'statusNotfound' 
            ]; 

            return $result;
        }

        $param = [
            'ext_response_data'  => [
                'reimburse'          => [
                    'status'             => $params['status'],
                    'message'            => $params['message'],
                    'mobile'             => @$params['mobile'],
                    'amount'             => @$params['amount'],
                    'ref_transaction_id' => $params['ref_transaction_id'],
                    'tmn_transaction_id' => @$params['tmn_transaction_id'],
                    'updated_date'       => $params['updated_date']
                ], 
            ] 
        ];
       
        foreach ($param as $key => $value) {
            
            if(is_array($value)){

                foreach ($value as $k => $v) {

                    if(is_array($v)){
                        
                        $model->{$key}->{$k} = $v;
                    }  
                }     
            }
            else{
                $model->{$key} = $value;
            }
        }
        
        if ($model->save()) {
            
            $result = [
                'success'            => true,
                'data'               => [
                    'status'             => $params['status'],
                    'message'            => $params['message'],
                    'mobile'             => @$params['mobile'],
                    'amount'             => @$params['amount'],
                    'ref_transaction_id' => $params['ref_transaction_id'],
                    'tmn_transaction_id' => @$params['tmn_transaction_id'],
                    'updated_date'       => $params['updated_date']
                ]
            ];
        }

        $message = $this->message->logmessage->reimburse_update;

        $response = $model->getOnlyData();
       
        $additional = ["request"=>$params,"response"=>$response,"relation_id"=>$model->{'tx_ref_id'},"endpoint"=>'/reimburse'];

        $graylog = $service->graylog($message,$additional,4,null,null,null,null);

        return $result;
    }


    protected function xmlFormat($params)
    {

        $isApplyCampaign = ($params['method'] == 'APPLYCAMPAIGN') ? true : false;
        $data            = '';
        $data .= '<?xml version="1.0" encoding="utf-8" ?>';
        $data .= '<request>';

        if ($isApplyCampaign) {
            $data .= '<Method>' . $params['method'] . '</Method>';
            $data .= '<OS>' . $params['OS'] . '</OS>';
            $data .= '<Parameter>';
            if (isset($params['campaign_code'])) {
                $data .= '<CampaignCode>' . $params['campaign_code'] . '</CampaignCode>';
                unset($params['campaign_code']);
            }
            unset($params['method']);
            unset($params['OS']);
        }

        foreach ($params as $key => $value) {
            $data .= '<' . $key . '>' . $value . '</' . $key . '>';
        }

        if ($isApplyCampaign) {
            $data .= '</Parameter>';
        }

        $data .= '</request>';

        return $data;
    }

    protected function convertData($typeConfig, $typeValue)
    {

        $result = '';

        if (isset($typeConfig[$typeValue])) {
            $result = $typeConfig[$typeValue];
        } else {
            $result = $typeConfig['default'];
        }

        return $result;
    }

    protected function insertTransaction($datas, $status, $type)
    {

        $model  = $this->getRedeemModel();
        $params = $this->mapKeyData($datas, $this->insertKey);

        $params['status']       = $status;
        $params['action']       = $type;
        $params['created_date'] = date('Y-m-d H:i:s');

        return $this->insertData($model, $params);
    }

    protected function updateStatusTransaction($id, $params, $status, $type, $channel)
    {

        $model = $this->getDataById($id); 
        $datetime = date('Y-m-d H:i:s');

        $document = [
            'action'            => $type,
            'status'            => $status,
            'ext_response_date' => $datetime,
            'ext_response_data' => $params['data'],
            'ext_ref_id'        => $params['data']['transaction_id'],
            'updated_date'      => $datetime,
            'channel'           => $channel,
        ];

        return $this->updateData($model, $document);
    }

    protected function mapKeyData($params, $configKey, $allowKey = [])
    {

        foreach ($params as $key => $value) {
            if (empty($allowKey)) {
                if (isset($configKey[$key])) {
                    $data[$configKey[$key]] = $value;
                } else {
                    $data[$key] = $value;
                }
            } else {
                if (in_array($key, $allowKey)) {
                    if (isset($configKey[$key])) {
                        $data[$configKey[$key]] = $value;
                    } else {
                        $data[$key] = $value;
                    }
                }
            }

            // else{
            //     $data[$key] = $value;
            // }
        }

        return $data;
    }

    protected function prepairDataForXml($configKey, $params)
    {

        $data = [];

        $data = $this->mapKeyData($params, $this->mapFeild);

        $data['method']  = $configKey['method'];
        $data['channel'] = $this->channel;

        if (isset($data['verify_type'])) {
            $data['verify_type'] = $this->convertData($this->configKeyAccType, $params['acc_type']);
        }

        $xmlFormat = $this->xmlFormat($data);

        return $xmlFormat;
    }

    protected function prepairRequestParams($params)
    {

        $data['message_id']  = $params['ext_ref_id'];
        $data['channel']     = $this->channel;
        $data['description'] = 'rollback point';
        return $data;
    }

    protected function prepairData($params, $configKey)
    {

        $data = [];

        if ($configKey['action'] == 'APPLY_CAMPAIGN') {
            $data = $params;
        } else {

            $data = $this->prepairDataForXml($configKey, $params);
        }

        return $data;
    }

    protected function getTransactionId($params, $method, $action)
    {

        if ($method == 'rollback') {
            $id = $params['id'];
        } else {
            $transaction = $this->insertTransaction($params, $this->statusType['new'], $action);
            $id          = (string) $transaction->_id;
        }

        return $id;
    }

    //method for redeem transaction
    protected function transactionProcess($params, $data, $configKey, $channel = '')
    {
        
        $outputs = [
            'success' => true,
            'message' => '',
            'data'    => [],
        ];
        $resVE        = ['verify' => null, 'earn' => null];
        $method       = $configKey['method'];
        $action       = $configKey['action'];
        $id           = $this->getTransactionId($params, $method, $action);
        $params['id'] = $id;
        
        try {
            //======= call service for curl trueyou  ========//
            $service         = $this->getServiceClass($configKey['service']);
            //switch to new ty endpoint case MERCHANT
            
            if(isset($params['campaign_type']) and 
                $params['campaign_type']=="MERCHANT") {
                $res             = $service->redeem_dmp($params);
                
            }else{
                $res             = $service->redeem($data, $params, $configKey);
            }

            $outputs['data'] = $res['data'];
            
        } catch (\Exception $e) {
            $outputs['success'] = false;
            $outputs['message'] = 'redeemFail';
            return $outputs;
        }

       
        if ($res['data']['code']!="200") {
            $this->status = $this->statusType['error'];
            if ($method == 'rollback') {
                $this->status = $this->statusType['rollback_fail'];
            }
        } else {

            $resReimburse = $this->reimburseMerchant($params,$res['data']['transaction_id']);

            $resRE = [];
            
            if (isset($resReimburse) && $resReimburse != '') {
                    
                $resRE = $resReimburse['reimburse']['data'];
            }

            try {
            
                $resVE        = $this->verify_earn($params, $res['data']['transaction_id']);
            } catch (\Exception $e) {
                $outputs['success'] = false;
                $outputs['message'] = 'verifyFail';
                return $outputs;
            }

            $this->status = $this->statusType['success'];
            if ($method == 'rollback') {
                $this->status = $this->statusType['rollback'];
            }
        }

                $outputs['data']['verify']    = $resVE['verify'];
                $outputs['data']['earn']      = $resVE['earn'];
                $outputs['data']['reimburse'] = $resRE;
            
                $res['data']['verify']    = $resVE['verify'];
                $res['data']['earn']      = $resVE['earn'];
                $res['data']['reimburse'] = $resRE;

        try {
            $update = $this->updateStatusTransaction(
                $id, $res, $this->status, $action, $channel
            );
        } catch (\Exception $e) {
            $outputs['success'] = false;
            $outputs['message'] = 'updateFail';
            return $outputs;
        }

        return $outputs;
    }

    protected function reimburseMerchant($params,$transaction_id)
    {

        $service   = $this->getServiceClass('redeem');
        $transactionService   = $this->getServiceClass('transaction');
        
        $imburseList = $service->chkReimburseConfig($params);

        $datetime = date('Y-m-d H:i:s');
        $output = [];
        
        if ($imburseList['status'] == true) {

            if(empty($params['mobile']) || $params['mobile'] == ''){
            
                $merchant = $transactionService->getMerchant($params);
                $merchantDetail = $transactionService->getMerchantDetail($merchant['data']);
                
                $params['mobile'] = @$merchantDetail['data']['0']['tmn']['qr_information']['truewallet_no'];

                if($params['mobile'] == ''){

                    $output = [
                        'reimburse'          => [
                            'status'             => false,
                            'data'               => [
                                'status'             => 'FAIL',
                                'message'            => 'Cannot Found TMN Wallet',
                                'updated_date'       => $datetime,
                            ]
                        ]
                    ]; 

                    return  $output;

                } 

            }

            $reqTopup = [
                'ref_transaction_id' => $transaction_id,
                'amount'             => $imburseList['imburse_amount'],
                'ref_code'           => $params['campaign_code'],
                'mobile'             => $params['mobile']
            ];
            
            $resTopup = $service->topupMerchant($reqTopup,$params['tx_ref_id']);
            
            if($resTopup['status']['code'] == 0){
                
                $output = [
                    'reimburse'          => [
                        'status'             => true,
                        'data'               => [
                            'status'             => 'SUCCESS',
                            'message'            => 'SUCCESS',
                            'mobile'             => $resTopup['data']['mobile'],
                            'amount'             => $resTopup['data']['amount'],
                            'transaction_id'     => $resTopup['data']['ref_transaction_id'],
                            'tmn_transaction_id' => $resTopup['data']['tmn_transaction_id'],
                            'updated_date'       => $resTopup['data']['tmn_success_datetime'],
                        ]
                    ]
                ];

            }
            else{
                $output = [
                    'reimburse'          => [
                        'status'             => false,
                        'data'               => [
                            'status'             => 'FAIL',
                            'message'            => $resTopup['status']['message'],
                            'updated_date'       => $datetime,
                        ]
                    ]
                ];   
            }
           
        } 
        
        return $output;
    }

    public function updateReimburseStatus($params)
    {
        $model = $this->getDataByTransactionId($params['ref_transaction_id']);
        $datetime = date('Y-m-d H:i:s');

        $param = [
            'status'             => $params['status'],
            'message'            => $params['message'],
            'mobile'             => @$params['mobile'],
            'amount'             => @$params['amount'],
            'ref_transaction_id' => $params['ref_transaction_id'],
            'tmn_transaction_id' => @$params['tmn_transaction_id'],
            'updated_date'       => $params['tmn_success_datetime']
        ];
        
        $result = $this->updateStatus($model, $param);
        
        return $result;
    }

    protected function filterSearchDataTransactions($params, $configKey)
    {

        $conditions = [];
        foreach ($params as $key => $value) {
            if (in_array($key, $configKey['searchFeild'])) {
                $conditions[$key] = $value;
            }
        }

        $conditions['action'] = $configKey['action'];

        return $conditions;
    }

    protected function checkAllowParams($params, $configKey)
    {
        $filter = [];
        $action = $params['action'];

        foreach ($params as $key => $value) {
            if (in_array($key, $configKey)) {
                $filter[$key] = $value;
            }
        }

        return $filter;
    }

    protected function formatParams($params, $method)
    {

        if ($method == 'APPLYCAMPAIGN') {
            if (isset($params['acc_type']) && isset($params['acc_value'])) {
                $params[$params['acc_type']] = $params['acc_value'];
                unset($params['acc_type']);
                unset($params['acc_value']);
            }
        }

        return $params;
    }

    protected function getParamForReversal($params, $transactionData, $allowKey, $action)
    {

        if ($action == 'apply_campaign') {
            $data       = $transactionData['ext_response_data']['response'];
            $data['id'] = $transactionData['id'];
        } else {
            $data = $this->mapKeyData($transactionData, array_flip($this->insertKey), $allowKey);
        }

        return $data;
    }

    //Method for get category detail
    protected function getDetailDataById($id)
    {
        //create model
        $model = $this->getRedeemModel();
        $users = $this->mongoService->getDetailDataById($model, $id, $this->allowFilter);

        return $users;
    }

    //Method for get detail
    protected function getDetailDataByIdLargeData($id, $collectionName)
    {
        //create model
        $datas = $this->mongoService->getDetailDataByIdLargeData($collectionName, $id);

        return $datas;
    }

    public function getOffset($limit, $page)
    {
        $offset = ($page - 1) * $limit;
        return $offset;
    }

    //Method for get data by filter
    protected function getDataByParams($params, $option = [], $allowFilter = [])
    {     
        //Create conditions
        $conditions = $this->mongoService->createConditionFilter($params, $allowFilter, $option);
        
        //create model
        $model = $this->getRedeemModel();
        $filterCon = [$conditions];
        
        //Manage order
        $filterCon = $this->mongoService->manageOrderInParams($params, $filterCon, $allowFilter);
        
        //query data
        $total = 0;
       
        if (isset($params['count'])) {

            $total = $model->count([$conditions]);
            return $total;

        } else {

            if (!isset($params['limit'])) {
                $params['limit'] = $this->defaultLimit;
            }

            if (!isset($params['page'])) {
                $params['page'] = $this->defaultPage;
            }

            if (!isset($params['offset'])) {
                $params['offset'] = $this->getOffset($params['limit'], $params['page']);
            }

            $total = $model->count([$conditions]);

            $filterCon = $this->mongoService->manageLimitOffsetInParams($params, $filterCon);

            $datas = $model->find($filterCon);

        }

        return [$datas, $total];
    }

    //Method for get tag by filter
    public function getTransaction($params)
    {
        //Define output
        $outputs = [
            'success' => true,
            'message' => '',
        ];

        try {
            $option = [];

            //create filter
            $tags = $this->getDataByParams($params, $option, $this->allowFilter);
            //get id from datas
            $ids = $this->mongoService->getAllIdFromDatas($tags[0]);

            $outputs['data'] = $ids;
        } catch (\Exception $e) {
            $outputs['success'] = false;
            $outputs['message'] = 'missionFail';
        }

        return $outputs;
    }

    public function countTransactionHistory($param)
    {

        //Define output
        $outputs = [
            'success' => true,
            'message' => '',
        ];
        try {
            //create filter

            $res     = $this->manageSeach($param);
            $params  = $res[0];
            $options = $res[1];
           
            $params = $this->mongoService->manageBetweenFilter($params, $options);

            $params['count'] = true;
            $redeem          = $this->getDataByParams($params, $options, $this->allowFilter);
            
            $outputs['data'] = $redeem;
        } catch (\Exception $e) {
            $outputs['success'] = false;
            $outputs['message'] = 'missionFail';
        }

        return $outputs;
    }

    //Method for get tag detail by id (list)
    public function getTransactionDetail($params)
    {
        //Define output
        $outputs = [
            'success' => true,
            'message' => '',
        ];

        try {
            $historys = $this->getDetailDataByIdLargeData($params['id'], 'redeem');

            $historys = $this->mongoService->manageSortDataByIdList($historys, $params['id']);

            $outputs['data'] = $historys;
        } catch (\Exception $e) {
            $outputs['success'] = false;
            $outputs['message'] = 'missionFail';
        }

        return $outputs;
    }

    protected function manageSeach($params)
    {
        
        $action = $this->actionList;

        if (isset($params['action']) && !empty($params['action'])) {
            $action = explode(',', $params['action']);
        }

        $params['action']  = $action;
        $options['action'] = '$in';

        //$params['options'] = $options;
        
        return [$params, $options];
    }

    //------- end: protected method ---------------//

    //Method for get user by filter

    public function getTransactionHistory($param)
    {
        
        // searh array 2d
        if(isset($param['reimburse_mobile'])){
            
            $param['ext_response_data.reimburse.mobile'] = $param['reimburse_mobile']; 
            unset($param['reimburse_mobile']);   
        }

        if(isset($param['reimburse_status'])){
            
            $param['ext_response_data.reimburse.status'] = strtoupper($param['reimburse_status']); 
            unset($param['reimburse_status']);   
        }
       
        //Define output
        $outputs = [
            'success' => true,
            'message' => '',
        ];
        try {
            //create filter

            $res     = $this->manageSeach($param);
            $params  = $res[0];
            $options = $res[1];
            
            $params = $this->mongoService->manageBetweenFilter($params, $options);
            
            $redeem = $this->getDataByParams($params, $options, $this->allowFilter);
           
            if (isset($param['id_only']) && $param['id_only']) {
                $datas = $this->mongoService->getAllIdFromDatas($redeem[0]);
            } else {

                $datas = $this->mongoService->addIdTodata($redeem[0]);
            }

            $outputs['data']        = $datas;

            $outputs['limit']       = isset($params['limit'])?$params['limit']:$this->defaultLimit;  
            $outputs['page']        = isset($params['page'])?$params['page']:$this->defaultPage;
            $outputs['totalRecord'] = isset($redeem[1])?$redeem[1]:0;

        } catch (\Exception $e) {
            $outputs['success'] = false;
            $outputs['message'] = 'missionFail';
        }
        
        return $outputs;
    }

    public function burnPoint($params)
    {
        
        $outputs = [
            'success' => true,
            'message' => '',
        ];
        $action = $params['action'];

        if (!isset($this->configKey['burn'][$action])) {
            $outputs['success'] = false;
            return $outputs;
        }

        $configKey   = $this->configKey['burn'][$action];
        
        $paramsAllow = $this->checkAllowParams(
            $params, $configKey['allowFeild']
        );

        $xmlData = $this->prepairDataForXml(
            $configKey,
            $this->formatParams($paramsAllow, $configKey['method'])
        );

        $outputs = $this->transactionProcess($paramsAllow, $xmlData, $configKey);

        return $outputs;
    }

    private function verify_earn($params, $transaction_id = "")
    {
        $output    = ['verify' => null, 'earn' => null];
        $service   = $this->getServiceClass('redeem');
        $BrandList = $service->chkAcceptBrand($params);
        if (!$BrandList) {
            return $output;
        }

        $reqVerify = [
            'campaign_code' => $params['campaign_code'],
            'reward_code'   => $params['reward_code'],
        ];
        $resVerify        = $service->verify($reqVerify);
        $output['verify'] = $resVerify;

        //status 100 = coupon was used
        if ($resVerify['data']['code'] == "200" and $resVerify['data']['status'] == "100") {
            $reqEarn = [
                'transaction_id' => $transaction_id,
                'channel'        => $BrandList['channel'],
                'action'         => $BrandList['action'],
                'msisdn'         => @$resVerify['data']['msisdn'],
                'thaiid'         => @$resVerify['data']['thai_id'],
                'multiplier'     => "1",
                'activity_date'  => date('Y-m-d H:i:s'),
                'ref1'           => "",
                'ref2'           => "",
                'ref3'           => "",
                'ref4'           => "",
                'ref5'           => "",
                'note'           => "",
            ];
            $resEarn = $service->earn($reqEarn);
        } else {
            $resEarn = [];
        }
        $output['earn'] = $resEarn;

        return $output;
    }

    public function rollback($params)
    {

        $action  = $params['action'];
        $channel = (isset($params['channel'])) ? $params['channel'] : '';

        $outputs = [
            'success' => true,
            'message' => '',
        ];

        if (!isset($this->configKey['rollback'][$action])) {
            $outputs['success'] = false;
            $outputs['message'] = 'actionNotfound';
            return $outputs;
        }

        if ($action == 'apply_campaign') {
            if (!in_array($channel, $this->channelRollback)) {
                $outputs['success'] = false;
                $outputs['message'] = 'channelNotfound';
                return $outputs;
            }
        }

        $configKey = $this->configKey['rollback'][$action];
        $params    = $this->checkAllowParams($params, $configKey['allowFeild']);

        $conditions = $this->filterSearchDataTransactions($params, $configKey);

        $transaction = $this->getTransaction($conditions);

        if (empty($transaction['data'])) {
            $outputs['data'] = null;
            return $outputs;
        }

        $transaction_detail = $this->getTransactionDetail(['id' => $transaction['data']]);

        $transactionData = $transaction_detail['data'][0];
        $status          = $transactionData['status'];

        if ($status == $this->statusType['rollback']) {
            $outputs['data'] = $transactionData['ext_response_data'];
            return $outputs;
        }

        $params = $this->getParamForReversal($params, $transactionData, $configKey['requiedFeildRollback'], $action);

        $res = $this->transactionProcess($params, $this->prepairData($params, $configKey), $configKey, $channel);

        $outputs['data'] = $res['data'];
        return $outputs;
    }

    //------- end: protected method ------------//
}
