<?php
use Phalcon\Loader;
use Phalcon\Cli\Console as ConsoleApp;
use Phalcon\Di\FactoryDefault\Cli as CliDI;
use Phalcon\Mvc\Collection\Manager;
use Phalcon\Db\Adapter\MongoDB\Client;

// Using the CLI factory default services container
$di = new CliDI();
//=== Start: Register class===
// Load the configuration file (if any)
$configFile = __DIR__ . "/config/config.php";
if (is_readable($configFile)) {
    $config = include $configFile;
    $di->set("config", $config);
}
//register repositories
$di->set('repository', function() {
    return new \App\Repositories\Repositories();
});

$di->set('collectionManager', function () {
    return new Manager();
}, true);

$di->set('mongoService', function() {
    return new \App\Services\MongoService();
});

$di->set('model', function() {
    return new \App\Models\Models();
});

//Initialise the mongo DB connection (original).
$di->setShared('mongo', function () {
    $config = $this->getShared('config');

    if (!$config->database->mongo->username || !$config->database->mongo->password) {
        $dsn = 'mongodb://' . $config->database->mongo->host.":". $config->database->mongo->port;
    } else {
        $dsn = sprintf(
            'mongodb://%s:%s@%s:%s/%s',
            $config->database->mongo->username,
            $config->database->mongo->password,
            $config->database->mongo->host,
            $config->database->mongo->port,
            $config->database->mongo->dbname
        );
    }

    $mongo = new \MongoDB\Driver\Manager($dsn);

    return $mongo;
});

// Initialise the mongo DB connection (original).
$di->setShared('mongoOrig', function () {
    $config = $this->getShared('config');

    if (!$config->database->mongo->username || !$config->database->mongo->password) {
        $dsn = 'mongodb://' . $config->database->mongo->host.":". $config->database->mongo->port;
    } else {
        $dsn = sprintf(
            'mongodb://%s:%s@%s:%s/%s',
            $config->database->mongo->username,
            $config->database->mongo->password,
            $config->database->mongo->host,
            $config->database->mongo->port,
            $config->database->mongo->dbname
        );
    }

    $mongo = new \MongoDB\Driver\Manager($dsn);

    return $mongo;
});

$di->set('collectionManager', function () {
    return new Manager();
}, true);

// Register a "myLibrary" service in the container
$di->set('myLibrary', function () {
    $myLib =  new App\Library\MyLibrary();
    return $myLib;
});

// Register a "mongoService" service in the container
$di->set('mongoService', function () {
    $myLib =  new App\Services\MongoService();
    return $myLib;
});
//=== End: Register class===


//=== Start: Load and set namespace ===
/**
 * Register the autoloader and tell it to register the tasks directory
 */
$loader = new Loader();
$loader->registerDirs(
    [
        __DIR__ . "/tasks",
        __DIR__ . "/repositories",
        __DIR__ . "/services",
        __DIR__ . "/models",
        __DIR__ . "/library",
    ]
);
$loader->registerNamespaces(array(
    'App\\Repositories' => __DIR__ . '/repositories/',
    'App\\Tasks'        => __DIR__ . '/tasks/',
    'App\\Services'        => __DIR__ . '/services/',
    'App\\Models'        => __DIR__ . '/models/',
    'App\\Library'         => __DIR__ . '/library/',
));
$loader->register();
//=== End: Load and set namespace ===
//Load vendor
include __DIR__.'/../vendor/autoload.php';
// Create a console application
$console = new ConsoleApp();
$console->setDI($di);
//=== Start: manage console arguments ===
/**
 * Process the console arguments
 */
$arguments = [];
foreach ($argv as $k => $arg) {
    if ($k === 1) {
        $arguments["task"] = $arg;
    } elseif ($k === 2) {
        $arguments["action"] = $arg;
    } elseif ($k >= 3) {
        $arguments["params"][] = $arg;
    }
}

date_default_timezone_set("Asia/Bangkok");

try {
    // Handle incoming arguments
    $console->handle($arguments);
} catch (\Phalcon\Exception $e) {
    echo $e->getMessage();
exit(255);
}
//=== End: manage console arguments ===