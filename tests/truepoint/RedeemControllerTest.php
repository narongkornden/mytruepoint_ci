<?php

use App\Controllers\RedeemController;
use Phalcon\DI;

class RedeemControllerTest extends UnitTestCase
{
    //------ start: MOCK DATA ---------//
    private $getDetailInputs = [
        'id' => '58eb5ab69aaf84001429e402,58eb5bc89aaf840010437512'
    ];

    //------ end: MOCK DATA ---------//

    //------- start: Method for support test --------//
    protected static function callMethod($obj, $name, array $args)
    {
        $class  = new \ReflectionClass($obj);
        $method = $class->getMethod($name);
        $method->setAccessible(true);

        return $method->invokeArgs($obj, $args);
    }
    //------- end: Method for support test --------//

    //------- start: Test function ---------//

    public function testGetRedeemRepository()
    {
        //mock repository
        $repository = Mockery::mock('Repository');
        $repository->shouldReceive("getRepository")->andReturn("TEST");

        //register
        $this->di->set('repository', $repository, true);

        //create class
        $user = new RedeemController();
        
        //call method
        $result   = $this->callMethod(
                $user,
                'getRedeemRepository',
               []
            );

        //check result
        $this->assertEquals( $result, "TEST" );
    }

     public function testGetSearchByHistoryActionError()
   {
        //create mock repo
        $truepointRepo = Mockery::mock('RedeemRepository');
        $truepointRepo->shouldReceive("getRedeem")->andReturn([
            'success' => false,
            'message' => 'missinFail',
        ]);
        //create class
        $user = $this->getMockBuilder('App\Controllers\RedeemController')
                           ->setMethods([
                                'getAllUrlParam',
                                'getRedeemRepository',
                                'getDataFromCache',
                                'validateBussinessError'
                            ])
                           ->getMock();

        $user->method('getAllUrlParam')
                   ->willReturn([]);

         $user->method('getDataFromCache')
                   ->willReturn([]);

        $user->method('getRedeemRepository')
                   ->willReturn($truepointRepo);

        $user->method('validateBussinessError')
                   ->willReturn([
                        'status' => [
                            'code'    => 400,
                            'message' => 'Bad request',
                        ],
                        'error' => [
                            "message" =>  "Mission Fail"
                        ]
                    ]);

        //call method
        $result = $user->getSearchByHistoryAction();

        //check result
        $this->assertInternalType('array', $result);
        $this->assertEquals($result['status']['code'], 400);
        $this->assertEquals($result['status']['message'], 'Bad request');
        $this->assertArrayHasKey('error', $result);
        $this->assertArrayHasKey('message', $result['error']);
        $this->assertEquals($result['error']['message'], 'Mission Fail');
    }

    public function testGetSearchByHistoryActionSuccess()
    {
        //create mock repo
        $truepointRepo = Mockery::mock('RedeemRepository');
        $truepointRepo->shouldReceive("getRedeem")->andReturn([
            'success' => true,
            'message' => '',
            'data'    => '58abd2f22f8331000a3acb92',
            'totalRecord' => '',
        ]);
        //create class
        $user = $this->getMockBuilder('App\Controllers\RedeemController')
                           ->setMethods([
                                'getAllUrlParam',
                                'getRedeemRepository',
                                'getDataFromCache',
                                'output'
                            ])
                           ->getMock();

        $user->method('getAllUrlParam')
                   ->willReturn([]);

        $user->method('getDataFromCache')
                   ->willReturn([]);

        $user->method('getRedeemRepository')
                   ->willReturn($truepointRepo);

        $user->method('output')
                   ->willReturn([
                        'status' => [
                            'code'    => 200,
                            'message' => 'Success',
                        ],
                        'data'    => '58abd2f22f8331000a3acb92',
            
                    ]);

        //call method
        $result = $user->getSearchByHistoryAction();

       

        //check result
        $this->assertInternalType('array', $result);
        $this->assertEquals($result['status']['code'], 200);
        $this->assertEquals($result['status']['message'], 'Success');
        $this->assertArrayHasKey('data', $result);
        $this->assertInternalType('string', $result['data']);
        $this->assertEquals('58abd2f22f8331000a3acb92', $result['data']);
    }



    public function testPostRedeemByCampaignActionValidateError(){
         //create class
        $user = $this->getMockBuilder('App\Controllers\RedeemController')
                           ->setMethods([
                                'getPostInput',
                                'validateApi',
                                'validateError'])
                           ->getMock();

        $user->method('getPostInput')
                   ->willReturn([]);

        $user->method('validateApi')
                   ->willReturn([
                        'msgError'   => 'The campaign_code is required',
                        'fieldError' => 'campaign_code'
                    ]);

        $user->method('validateError')
                   ->willReturn([
                        'status' => [
                            'code'    => 400,
                            'message' => 'Bad Request',
                        ],
                        'error' => [
                            'message'  => 'The campaign_code is required',
                            "property" => "campaign_code"
                        ]
                    ]);

        //call method
        $result = $user->postRedeemByCampaignAction();

        //check result
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('status', $result);
        $this->assertInternalType('array', $result['status']);
        $this->assertArrayHasKey('code', $result['status']);
        $this->assertArrayHasKey('message', $result['status']);
        $this->assertEquals($result['status']['code'], 400);
        $this->assertEquals($result['status']['message'], 'Bad Request');
        $this->assertArrayHasKey('error', $result);
        $this->assertInternalType('array', $result['error']);
        $this->assertArrayHasKey('message', $result['error']);
        $this->assertEquals($result['error']['message'], 'The campaign_code is required');
        $this->assertArrayHasKey('property', $result['error']);
        $this->assertEquals($result['error']['property'], 'campaign_code');

    }

    public function testPostRedeemByCampaignActionError(){
         //create class
        $burnpointRepo = Mockery::mock('RedeemRepository');
        $burnpointRepo->shouldReceive("redeemByCampaign")->andReturn([
            'success' => false,
            'message' => 'burnError',
        ]);
        //create class
        $user = $this->getMockBuilder('App\Controllers\RedeemController')
                           ->setMethods([
                                'getPostInput',
                                'validateApi',
                                'getRedeemRepository',
                                'validateBussinessError'
                            ])
                           ->getMock();

        $user->method('getPostInput')
                   ->willReturn([]);

         $user->method('validateApi')
                   ->willReturn(['']);

        $user->method('getRedeemRepository')
                   ->willReturn($burnpointRepo);


        $user->method('validateBussinessError')
                   ->willReturn([
                        'status' => [
                            'code'    => 400,
                            'message' => 'Bad request',
                        ],
                        'error' => [
                            "message" =>  "Cannot earn point"
                        ]
                    ]);

        //call method
        $result = $user->postRedeemByCampaignAction();

        //check result
        $this->assertInternalType('array', $result);
        $this->assertEquals($result['status']['code'], 400);
        $this->assertEquals($result['status']['message'], 'Bad request');
        $this->assertArrayHasKey('error', $result);
        $this->assertArrayHasKey('message', $result['error']);
        $this->assertEquals($result['error']['message'], 'Cannot earn point');

    }

    public function testPostRedeemByCampaignActionSuccess()
    {
        $cacheService = Mockery::mock('CacheService');
        $cacheService->shouldReceive('deleteCacheByPrefix')->andReturn(null);

        //cache
        $this->di->set('cacheService', $cacheService, true);

         //create class
        $burnpointRepo = Mockery::mock('RedeemRepository');
        $burnpointRepo->shouldReceive("redeemByCampaign")->andReturn([
            'success' => true,
            'message' => '',
            'data'  => [
                "code"               => "200",
                "description"        => "success",
                "ext_transaction_id" => "edc-0000-00030",
                "service_desc"       => "Truecard::request_benefit",
                "method"             => "request_benefit",
                "channel"            => "RPP",
                "transaction_id"     => "3103125777f17dw-stagea01RPPedc-0000-00030",
                "point_balance"      => "19900",
                "moreinfo"           => [
                  "promotion_code" => "TY24492",
                  "message"        => [],
                  "point_remain"   => "19900",
                  "point_action"   => "N",
                  "point_redeem"   => "0"
                ],
            ]
        ]);
        //create class
        $user = $this->getMockBuilder('App\Controllers\RedeemController')
                           ->setMethods([
                                'getPostInput',
                                'validateApi',
                                'getRedeemRepository',
                                'output'
                            ])
                           ->getMock();

        $user->method('getPostInput')
                   ->willReturn([]);

         $user->method('validateApi')
                   ->willReturn(['']);

        $user->method('getRedeemRepository')
                   ->willReturn($burnpointRepo);


        $user->method('output')
                   ->willReturn([
                        'status' => [
                            'code'    => 200,
                            'message' => 'Success',
                        ],
                        'data'  => [
                            "code"               => "200",
                            "description"        => "success",
                            "ext_transaction_id" => "edc-0000-00030",
                            "service_desc"       => "Truecard::request_benefit",
                            "method"             => "request_benefit",
                            "channel"            => "RPP",
                            "transaction_id"     => "3103125777f17dw-stagea01RPPedc-0000-00030",
                            "point_balance"      => "19900",
                            "moreinfo"           => [
                              "promotion_code" => "TY24492",
                              "message"        => [],
                              "point_remain"   => "19900",
                              "point_action"   => "N",
                              "point_redeem"   => "0"
                            ],
                        ]
                    ]);

        //call method
        $result = $user->postRedeemByCampaignAction();

        //check result
        $this->assertInternalType('array', $result);
        $this->assertEquals($result['status']['code'], 200);
        $this->assertEquals($result['status']['message'], 'Success');
        $this->assertArrayHasKey('data', $result);

    }

    public function testPostRedeemByCodeActionValidateError(){
         //create class
        $user = $this->getMockBuilder('App\Controllers\RedeemController')
                           ->setMethods([
                                'getPostInput',
                                'validateApi',
                                'validateError'])
                           ->getMock();

        $user->method('getPostInput')
                   ->willReturn([]);

        $user->method('validateApi')
                   ->willReturn([
                        'msgError'   => 'The campaign_code is required',
                        'fieldError' => 'campaign_code'
                    ]);

        $user->method('validateError')
                   ->willReturn([
                        'status' => [
                            'code'    => 400,
                            'message' => 'Bad Request',
                        ],
                        'error' => [
                            'message'  => 'The campaign_code is required',
                            "property" => "campaign_code"
                        ]
                    ]);

        //call method
        $result = $user->postRedeemByCodeAction();

        //check result
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('status', $result);
        $this->assertInternalType('array', $result['status']);
        $this->assertArrayHasKey('code', $result['status']);
        $this->assertArrayHasKey('message', $result['status']);
        $this->assertEquals($result['status']['code'], 400);
        $this->assertEquals($result['status']['message'], 'Bad Request');
        $this->assertArrayHasKey('error', $result);
        $this->assertInternalType('array', $result['error']);
        $this->assertArrayHasKey('message', $result['error']);
        $this->assertEquals($result['error']['message'], 'The campaign_code is required');
        $this->assertArrayHasKey('property', $result['error']);
        $this->assertEquals($result['error']['property'], 'campaign_code');

    }

    public function testPostRedeemByCodeActionError(){
         //create class
        $burnpointRepo = Mockery::mock('RedeemRepository');
        $burnpointRepo->shouldReceive("redeemByCode")->andReturn([
            'success' => false,
            'message' => 'burnError',
        ]);
        //create class
        $user = $this->getMockBuilder('App\Controllers\RedeemController')
                           ->setMethods([
                                'getPostInput',
                                'validateApi',
                                'getRedeemRepository',
                                'validateBussinessError'
                            ])
                           ->getMock();

        $user->method('getPostInput')
                   ->willReturn([]);

         $user->method('validateApi')
                   ->willReturn(['']);

        $user->method('getRedeemRepository')
                   ->willReturn($burnpointRepo);


        $user->method('validateBussinessError')
                   ->willReturn([
                        'status' => [
                            'code'    => 400,
                            'message' => 'Bad request',
                        ],
                        'error' => [
                            "message" =>  "Cannot earn point"
                        ]
                    ]);

        //call method
        $result = $user->postRedeemByCodeAction();

        //check result
        $this->assertInternalType('array', $result);
        $this->assertEquals($result['status']['code'], 400);
        $this->assertEquals($result['status']['message'], 'Bad request');
        $this->assertArrayHasKey('error', $result);
        $this->assertArrayHasKey('message', $result['error']);
        $this->assertEquals($result['error']['message'], 'Cannot earn point');

    }

    public function testPostRedeemByCodeActionSuccess()
    {
        $cacheService = Mockery::mock('CacheService');
        $cacheService->shouldReceive('deleteCacheByPrefix')->andReturn(null);

        //cache
        $this->di->set('cacheService', $cacheService, true);
         //create class
        $burnpointRepo = Mockery::mock('RedeemRepository');
        $burnpointRepo->shouldReceive("redeemByCode")->andReturn([
            'success' => true,
            'message' => '',
            'data'  => [
                "code"               => "200",
                "description"        => "success",
                "ext_transaction_id" => "edc-0000-00030",
                "service_desc"       => "Truecard::request_benefit",
                "method"             => "request_benefit",
                "channel"            => "RPP",
                "transaction_id"     => "3103125777f17dw-stagea01RPPedc-0000-00030",
                "point_balance"      => "19900",
                "moreinfo"           => [
                  "promotion_code" => "TY24492",
                  "message"        => [],
                  "point_remain"   => "19900",
                  "point_action"   => "N",
                  "point_redeem"   => "0"
                ],
            ]
        ]);
        //create class
        $user = $this->getMockBuilder('App\Controllers\RedeemController')
                           ->setMethods([
                                'getPostInput',
                                'validateApi',
                                'getRedeemRepository',
                                'output'
                            ])
                           ->getMock();

        $user->method('getPostInput')
                   ->willReturn([]);

         $user->method('validateApi')
                   ->willReturn(['']);

        $user->method('getRedeemRepository')
                   ->willReturn($burnpointRepo);


        $user->method('output')
                   ->willReturn([
                        'status' => [
                            'code'    => 200,
                            'message' => 'Success',
                        ],
                        'data'  => [
                            "code"               => "200",
                            "description"        => "success",
                            "ext_transaction_id" => "edc-0000-00030",
                            "service_desc"       => "Truecard::request_benefit",
                            "method"             => "request_benefit",
                            "channel"            => "RPP",
                            "transaction_id"     => "3103125777f17dw-stagea01RPPedc-0000-00030",
                            "point_balance"      => "19900",
                            "moreinfo"           => [
                              "promotion_code" => "TY24492",
                              "message"        => [],
                              "point_remain"   => "19900",
                              "point_action"   => "N",
                              "point_redeem"   => "0"
                            ],
                        ]
                    ]);

        //call method
        $result = $user->postRedeemByCodeAction();

        //check result
        $this->assertInternalType('array', $result);
        $this->assertEquals($result['status']['code'], 200);
        $this->assertEquals($result['status']['message'], 'Success');
        $this->assertArrayHasKey('data', $result);

    }

     public function testpostRedeemReversalActionValidateError(){
         
         //create class repo
        $burnpointRepo = Mockery::mock('RedeemRepository');
        $burnpointRepo->shouldReceive("redeemReversal")->andReturn(['Test']);

         //create class
        $user = $this->getMockBuilder('App\Controllers\RedeemController')
                           ->setMethods([
                                'getPostInput',
                                'validateApi',
                                'validateError'])
                           ->getMock();

        $user->method('getPostInput')
                   ->willReturn([]);

        $user->method('validateApi')
                   ->willReturn([
                        'msgError'   => 'The campaign_code is required',
                        'fieldError' => 'campaign_code'
                    ]);

        $user->method('validateError')
                   ->willReturn([
                        'status' => [
                            'code'    => 400,
                            'message' => 'Bad Request',
                        ],
                        'error' => [
                            'message'  => 'The campaign_code is required',
                            "property" => "campaign_code"
                        ]
                    ]);

        //call method
        $result = $user->postRedeemReversalAction();

        //check result
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('status', $result);
        $this->assertInternalType('array', $result['status']);
        $this->assertArrayHasKey('code', $result['status']);
        $this->assertArrayHasKey('message', $result['status']);
        $this->assertEquals($result['status']['code'], 400);
        $this->assertEquals($result['status']['message'], 'Bad Request');
        $this->assertArrayHasKey('error', $result);
        $this->assertInternalType('array', $result['error']);
        $this->assertArrayHasKey('message', $result['error']);
        $this->assertEquals($result['error']['message'], 'The campaign_code is required');
        $this->assertArrayHasKey('property', $result['error']);
        $this->assertEquals($result['error']['property'], 'campaign_code');

    }

    public function testpostRedeemReversalActionError(){
         //create class
        $burnpointRepo = Mockery::mock('RedeemRepository');
        $burnpointRepo->shouldReceive("redeemReversal")->andReturn([
            'success' => false,
            'message' => 'burnError',
        ]);
        //create class
        $user = $this->getMockBuilder('App\Controllers\RedeemController')
                           ->setMethods([
                                'getPostInput',
                                'validateApi',
                                'getRedeemRepository',
                                'validateBussinessError'
                            ])
                           ->getMock();

        $user->method('getPostInput')
                   ->willReturn([]);

         $user->method('validateApi')
                   ->willReturn(['']);

        $user->method('getRedeemRepository')
                   ->willReturn($burnpointRepo);


        $user->method('validateBussinessError')
                   ->willReturn([
                        'status' => [
                            'code'    => 400,
                            'message' => 'Bad request',
                        ],
                        'error' => [
                            "message" =>  "Cannot earn point"
                        ]
                    ]);

        //call method
        $result = $user->postRedeemReversalAction();

        //check result
        $this->assertInternalType('array', $result);
        $this->assertEquals($result['status']['code'], 400);
        $this->assertEquals($result['status']['message'], 'Bad request');
        $this->assertArrayHasKey('error', $result);
        $this->assertArrayHasKey('message', $result['error']);
        $this->assertEquals($result['error']['message'], 'Cannot earn point');

    }

    public function testpostRedeemReversalActionSuccess()
    {
        $cacheService = Mockery::mock('CacheService');
        $cacheService->shouldReceive('deleteCacheByPrefix')->andReturn(null);

        //cache
        $this->di->set('cacheService', $cacheService, true);
         //create class
        $burnpointRepo = Mockery::mock('RedeemRepository');
        $burnpointRepo->shouldReceive("redeemReversal")->andReturn([
            'success' => true,
            'message' => '',
            'data'  => [
                "code"               => "200",
                "description"        => "success",
                "ext_transaction_id" => "edc-0000-00030",
                "service_desc"       => "Truecard::request_benefit",
                "method"             => "request_benefit",
                "channel"            => "RPP",
                "transaction_id"     => "3103125777f17dw-stagea01RPPedc-0000-00030",
                "point_balance"      => "19900",
                "moreinfo"           => [
                  "promotion_code" => "TY24492",
                  "message"        => [],
                  "point_remain"   => "19900",
                  "point_action"   => "N",
                  "point_redeem"   => "0"
                ],
            ]
        ]);
        //create class
        $user = $this->getMockBuilder('App\Controllers\RedeemController')
                           ->setMethods([
                                'getPostInput',
                                'validateApi',
                                'getRedeemRepository',
                                'output'
                            ])
                           ->getMock();

        $user->method('getPostInput')
                   ->willReturn([]);

         $user->method('validateApi')
                   ->willReturn(['']);

        $user->method('getRedeemRepository')
                   ->willReturn($burnpointRepo);


        $user->method('output')
                   ->willReturn([
                        'status' => [
                            'code'    => 200,
                            'message' => 'Success',
                        ],
                        'data'  => [
                            "code"               => "200",
                            "description"        => "success",
                            "ext_transaction_id" => "edc-0000-00030",
                            "service_desc"       => "Truecard::request_benefit",
                            "method"             => "request_benefit",
                            "channel"            => "RPP",
                            "transaction_id"     => "3103125777f17dw-stagea01RPPedc-0000-00030",
                            "point_balance"      => "19900",
                            "moreinfo"           => [
                              "promotion_code" => "TY24492",
                              "message"        => [],
                              "point_remain"   => "19900",
                              "point_action"   => "N",
                              "point_redeem"   => "0"
                            ],
                        ]
                    ]);

        //call method
        $result = $user->postRedeemReversalAction();
        
        //check result
        $this->assertInternalType('array', $result);
        $this->assertEquals($result['status']['code'], 200);
        $this->assertEquals($result['status']['message'], 'Success');
        $this->assertArrayHasKey('data', $result);

    }



    //------- end: Test function ---------//
}
