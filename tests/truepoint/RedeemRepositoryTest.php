<?php

use Phalcon\DI;
use App\Repositories\RedeemRepository;

class RedeemRepositoryTest extends UnitTestCase
{
    //------ start: MOCK DATA ---------//
    protected $statusType = [
        'new'      => 'new',
        'success'  => 'success',
        'waiting'  => 'waiting',
        'error'    => 'fail',
        'rollback' => 'rollback'
    ];
    protected $redeemType = 'default';
    //------ end: MOCK DATA ---------//

    //------- start: Method for support test --------//
    protected static function callMethod($obj, $name, array $args)
    {
        $class  = new \ReflectionClass($obj);
        $method = $class->getMethod($name);
        $method->setAccessible(true);

        return $method->invokeArgs($obj, $args);
    }
    //------- end: Method for support test --------//

    //------- start: Test function ---------//
    public function testGetRedeemsModel()
    {
        //Mock model
        $model = Mockery::mock('Model');
        $model->shouldReceive('getModel')->andReturn("TEST");

        //register model
        $this->di->set('model', $model, true);

        //create class
        $repo = new RedeemRepository();

        //call method
        $result = $this->callMethod(
            $repo,
            'getRedeemModel',
            []
        );

        //check result
        $this->assertEquals($result, 'TEST');
    }

    public function testInsertRedeemError()
    {
        //mock model
        $model = Mockery::mock('Redeem');
        $model->shouldReceive("save")->andReturn(false);

        //create class
        //$repo = new RedeemRepository();

        $repo = $this->getMockBuilder('App\Repositories\RedeemRepository')
                    ->setMethods(['getRedeemModel'])
                    ->getMock();

        $repo->method('getRedeemModel')
            ->willReturn($model);

        
        
        //default
        $params = [
            [
                "tx_ref_id"     => "655ab2c",
                "brand_id"      => "801acb59",
                "outlet_id"     => "6900006",
                "terminal_id"   => "87ab6e2f",
                "acc_type"      => "merchant",
                "acc_value"     => "20",
                "campaign_code" => "9ba2be3",
                "reward_code"   => "",
                
            ],
            "status"        => "merchant",
            "action"        => "sale"
        ];
        //call method
        $result = $this->callMethod(
            $repo,
            'InsertRedeem',
            $params
        );

        //check result
        $this->assertEquals($result, false);

    }

    public function testInsertRedeemSuccess()
    {
        //mock model
        $model = Mockery::mock('Redeem');
        $model->shouldReceive("save")->andReturn(true);
        $model->shouldReceive('getOnlyData')->andReturn($model);

        //create class
        //$repo = new RedeemRepository();

        $repo = $this->getMockBuilder('App\Repositories\RedeemRepository')
                    ->setMethods(['getRedeemModel'])
                    ->getMock();

        $repo->method('getRedeemModel')
            ->willReturn($model);

        
        
        //default
        $params = [
            [
                "tx_ref_id"     => "655ab2c",
                "brand_id"      => "801acb59",
                "outlet_id"     => "6900006",
                "terminal_id"   => "87ab6e2f",
                "acc_type"      => "merchant",
                "acc_value"     => "20",
                "campaign_code" => "9ba2be3",
                "reward_code"   => "",
                
            ],
            "status"        => "merchant",
            "action"        => "sale"
        ];
        //call method
        $result = $this->callMethod(
            $repo,
            'insertRedeem',
            $params
        );
        //check result
         $this->assertNotNull($result);


    }

    public function testGetDataById()
    {
        //Mock model
        $model = Mockery::mock('Message');
        $model->shouldReceive('findById')->andReturn('Test');
        
        //create class
        $repo = $this->getMockBuilder('App\Repositories\RedeemRepository')
                           ->setMethods([
                                'getRedeemModel'])
                           ->getMock();
                           
        $repo->method('getRedeemModel')
                   ->willReturn($model);


        $result = $this->callMethod(
            $repo,
            'getDataById',
            ['1']
        );

        $this->assertEquals('Test', $result);
    }

    public function testUpdateStatusError()
    {
        //default
        $dataMock['data'] =  [
                "tx_ref_id"     => "655ab2c",
                "brand_id"      => "801acb59",
                "outlet_id"     => "6900006",
                "terminal_id"   => "87ab6e2f",
                "acc_type"      => "merchant",
                "acc_value"     => "20",
                "transaction_id"=> "82ecab325b",
                "campaign_code" => "9ba2be3",
                "reward_code"   => "",
                ];
        
        //mock model
        $model = Mockery::mock('Redeem');
        $model->shouldReceive("save")->andReturn(false);
        


        $repo = $this->getMockBuilder('App\Repositories\RedeemRepository')
                    ->setMethods(['getRedeemById','updateData'])
                    ->getMock();

        $repo->method('getRedeemById')
            ->willReturn($model);

       
        $params = [
                '59004d1b62c260000f228a45',
                $dataMock,
                'merchant',
                'sale'];
        //call method
        $result = $this->callMethod(
            $repo,
            'updateStatusRedeem',
            $params
        );

        //check result
        $this->assertEquals($result, false);

    }

    public function testUpdateStatusSuccess()
    {
        //default
        $dataMock['data'] =  [
                "tx_ref_id"     => "655ab2c",
                "brand_id"      => "801acb59",
                "outlet_id"     => "6900006",
                "terminal_id"   => "87ab6e2f",
                "acc_type"      => "merchant",
                "acc_value"     => "20",
                "transaction_id"=> "82ecab325b",
                "campaign_code" => "9ba2be3",
                "reward_code"   => "",
                ];
        //mock model
        $model = Mockery::mock('Redeem');
        $model->shouldReceive("save")->andReturn(true);
        $model->retry = 1;


        $repo = $this->getMockBuilder('App\Repositories\RedeemRepository')
                    ->setMethods(['getRedeemById','updateData'])
                    ->getMock();

        $repo->method('getRedeemById')
            ->willReturn($model);

        $repo->method('updateData')
            ->willReturn($model);
    
        $params = [
                '59004d1b62c260000f228a45',
                $dataMock,
                'merchant',
                'sale'];
        //call method
        $result = $this->callMethod(
            $repo,
            'updateStatusRedeem',
            $params
        );

        //check result
        $this->assertEquals($result, $model);

    }

     public function testGetServiceClass()
    {
        //Mock service
        $service = Mockery::mock('Service');
        $service->shouldReceive('getService')->andReturn("TEST");

        //register service
        $this->di->set('service', $service, true);

        //create class
        $repo = new RedeemRepository();

        //call method
        $result = $this->callMethod(
            $repo,
            'getServiceClass',
            []
        );

        //check result
        $this->assertEquals($result, 'TEST');
    }

    public function testXmlFormatIfEqualMark_use()
    {
        //default
        $params = [
            "tx_ref_id"     => "655ab2c",
            "brand_id"      => "801acb59",
            "outlet_id"     => "6900006",
            "terminal_id"   => "87ab6e2f",
            "acc_type"      => "merchant",
            "acc_value"     => "20",
            "campaign_code" => "9ba2be3",
            "reward_code"   => "",   
            "action"        => "rollback"          
        ];

        $ansMockForThisMethod = '<?xml version="1.0" encoding="utf-8" ?><request><method>mark_use</method><channel>RPP</channel><terminal_id>87ab6e2f</terminal_id><verify_type>merchant</verify_type><verify_value>20</verify_value><campaign_code>9ba2be3</campaign_code><ext_transaction_id>655ab2c</ext_transaction_id><promotion_code></promotion_code></request>';


        //create class
        $repo = $this->getMockBuilder('App\Repositories\RedeemRepository')
                    ->setMethods([
                        'convertData'
                        ])
                    ->getMock();

        $repo->method('convertData')
            ->willReturn($params);

        //call method
        $result = $this->callMethod(
            $repo,
            'xmlFormat',
            ['mark_use',$params]
        );
        $this->assertEquals($ansMockForThisMethod, $result);
    }

    public function testXmlFormatIfEqualRollback()
    {
        //default
        $params = [
            "tx_ref_id"      => "655ab2c",
            "brand_id"       => "801acb59",
            "outlet_id"      => "6900006",
            "terminal_id"    => "87ab6e2f",
            "acc_type"       => "merchant",
            "acc_value"      => "20",
            "campaign_code"  => "9ba2be3",
            "transaction_id" => "905c6ab23e",
            "reason_rollback"=> "50a0bc",
             "action"        => "rollback"               
        ];

        $ansMockForThisMethod = '<?xml version="1.0" encoding="utf-8" ?><request><method>rollback</method><channel>RPP</channel><terminal_id>87ab6e2f</terminal_id><verify_type>merchant</verify_type><verify_value>20</verify_value><campaign_code>9ba2be3</campaign_code><ext_transaction_id>655ab2c</ext_transaction_id><reason_rollback> Reversal rollback</reason_rollback><transaction_id>905c6ab23e</transaction_id></request>';

        //create class
        $repo = $this->getMockBuilder('App\Repositories\RedeemRepository')
                    ->setMethods([
                        'convertData'
                        ])
                    ->getMock();

        $repo->method('convertData')
            ->willReturn($params);

        //call method
        $result = $this->callMethod(
            $repo,
            'xmlFormat',
            ['rollback',$params]
        );
        $this->assertEquals($ansMockForThisMethod, $result);
    }

    public function testConvertDataAcc_TypeEqualCardid()
    {
        //default
        $params['acc_type'] = 'CARD';

        //create class
        $repo = new RedeemRepository();

        //call method
        $result = $this->callMethod(
            $repo,
            'convertData',
            [$params]
        );

        $this->assertEquals('cardid', $result['acc_type']);

    }

    public function testConvertDataAcc_TypeEqualThaiid()
    {
        //default
        $params['acc_type'] = 'THAIID';

        //create class
        $repo = new RedeemRepository();

        //call method
        $result = $this->callMethod(
            $repo,
            'convertData',
            [$params]
        );

        $this->assertEquals('thaiid', $result['acc_type']);

    }

    public function testConvertDataAcc_TypeEqualProductid()
    {
        //default
        $params['acc_type'] = 'MOBILE';

        //create class
        $repo = new RedeemRepository();

        //call method
        $result = $this->callMethod(
            $repo,
            'convertData',
            [$params]
        );

        $this->assertEquals('productid', $result['acc_type']);

    }

    public function testConvertDataAcc_TypeEqualDefault()
    {
        //default
        $params['acc_type'] = 'merchant';

        //create class
        $repo = new RedeemRepository();

        //call method
        $result = $this->callMethod(
            $repo,
            'convertData',
            [$params]
        );

        $this->assertEquals('merchant', $result['acc_type']);

    }

    public function testGetTransactionMissionFail()
    {
        $RedeemRepository = new RedeemRepository();

        $result = $RedeemRepository->getTransaction([]);
        
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('success', $result);
        $this->assertArrayHasKey('message', $result);
        $this->assertEquals(false,$result['success']);
        $this->assertEquals('missionFail', $result['message']);
    }

    public function testGetTransactionSuccess()
    {
        //Mock type
        $model = Mockery::mock('Redeem');

        //Mock mongo services
        $mongoService = Mockery::mock('MongoService');
        $mongoService->shouldReceive('getAllIdFromDatas')->andReturn('590008bb1d2dc901014049c9');

        //register model
        $this->di->set('mongoService', $mongoService, true);
        //creste class
        $repo = $this->getMockBuilder('App\Repositories\RedeemRepository')
                    ->setMethods([
                        'getDataByParams',
                        'getAllIdFromDatas
                        '])
                    ->getMock();

        $repo->method('getDataByParams')
            ->willReturn([$model]);


        //call method 
        $result = $repo->getTransaction([]);

        //check result
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('success', $result);
        $this->assertArrayHasKey('message', $result);
        $this->assertTrue($result['success']);
        $this->assertArrayHasKey('data', $result);
        $this->assertInternalType('string', $result['data']);
        $this->assertEquals('590008bb1d2dc901014049c9', $result['data']);

        
    }

    public function testGetDataByParamsNoLimit()
    {
        //mock mongoService
        $mongoService = Mockery::mock('MongoService');
        $mongoService->shouldReceive('createConditionFilter')->andReturn([
            'name' => ['$ne' => 'action']
        ]);
        $mongoService->shouldReceive('manageOrderInParams')->andReturn([[
            'name' => ['$ne' => 'action']
        ]]);
        
        //register mongoService
        $this->di->set('mongoService', $mongoService, true);

        //Mock model
        $model = Mockery::mock('Model');
        $model->shouldReceive('find')->andReturn([
            [
                "tx_ref_id"     => "655ab2c",
                "brand_id"      => "801acb59",
                "outlet_id"     => "6900006",
                "terminal_id"   => "87ab6e2f",
                "account_type"  => "merchant",
                "account_value" => "20",
                "campaign_code" => "9ba2be3",
                "id"            => "590008bb1d2dc901014049c9"
            ]
        ]);


        //create repo
        $repo = $this->getMockBuilder('App\Repositories\RedeemRepository')
                           ->setMethods([
                                'getRedeemModel'])
                           ->getMock();

        $repo->method('getRedeemModel')
                   ->willReturn($model);

        //create params
        $params = [['name' => 'Tes%'], 'en'];

        //call method
        $result = $this->callMethod(
            $repo,
            'getDataByParams',
            $params
        );

        //check result
        $this->assertInternalType('array', $result);
        $this->assertInternalType('array', $result[0]);
        $this->assertInternalType('integer', $result[1]);
        $this->assertEquals(1, $result[1]);
        $this->assertEquals('590008bb1d2dc901014049c9', $result[0][0]['id']);

    }

    public function testGetDataByParamsHaveLimit()
    {
        //mock mongoService
        $mongoService = Mockery::mock('MongoService');
        $mongoService->shouldReceive('createConditionFilter')->andReturn([
            'username' => ['$ne' => 'action']
        ]);
        $mongoService->shouldReceive('manageLimitOffsetInParams')->andReturn([
            'username' => ['$ne' => 'action'], 'limit' => 3, 'skip' => 0
        ]);
        $mongoService->shouldReceive('manageOrderInParams')->andReturn([[
            'username' => ['$ne' => 'action'], 'limit' => 3, 'skip' => 0
        ]]);

        //register mongoService
        $this->di->set('mongoService', $mongoService, true);

        //Mock model
        $model = Mockery::mock('Model');
        $model->shouldReceive('find')->andReturn([
            [
                "tx_ref_id"     => "655ab2c",
                "brand_id"      => "801acb59",
                "outlet_id"     => "6900006",
                "terminal_id"   => "87ab6e2f",
                "account_type"  => "merchant",
                "account_value" => "20",
                "campaign_code" => "9ba2be3",
                "id"            => "590008bb1d2dc901014049c9"
            ]
        ]);

        $model->shouldReceive('count')->andReturn(1);


        //create repo
        $repo = $this->getMockBuilder('App\Repositories\RedeemRepository')
                           ->setMethods([
                                'getRedeemModel'])
                           ->getMock();
                           
        $repo->method('getRedeemModel')
                   ->willReturn($model);

        //create params
        $params = [['content_type' => 'cou%', 'limit' => 3]];

        //call method
        $result = $this->callMethod(
            $repo,
            'getDataByParams',
            $params
        );

        //check result
        $this->assertInternalType('array', $result);
        $this->assertInternalType('array', $result[0]);
        $this->assertInternalType('integer', $result[1]);
        $this->assertEquals(1, $result[1]);
        $this->assertEquals('590008bb1d2dc901014049c9', $result[0][0]['id']);
    }

    public function testGetDetailDataById()
    {
        //Mock user
        $model = Mockery::mock('Redeem');

        //Mock mongoService
        $mongoService = Mockery::mock('MongoService');
        $mongoService->shouldReceive('getDetailDataById')->andReturn($model);

        //register repository
        $this->di->set('mongoService', $mongoService, true);

        //create repo
        $repo = $this->getMockBuilder('App\Repositories\RedeemRepository')
                           ->setMethods([
                                'getRedeemModel'])
                           ->getMock();
                           
        $repo->method('getRedeemModel')
                   ->willReturn($model);


        //create params
        $params = ['58e27db58d6a71405dbbdb32'];

        //call method
        $result = $this->callMethod(
            $repo,
            'getDetailDataById',
            $params
        );

        //check result
        $this->assertInternalType('object', $result);
        $this->assertNotNull($result);
    }

    public function testgetTransactionDetailException()
    {
        //create class
        $repo = new RedeemRepository();

        //call method 
        $result = $repo->getTransactionDetail([]);

        //check result
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('success', $result);
        $this->assertArrayHasKey('message', $result);
        $this->assertFalse($result['success']);
        $this->assertEquals('missionFail', $result['message']);

    }

    public function testGetTransactionDetailSuccess()
    {
        //Mocke category
        $tag = Mockery::mock('Redeem');

        //Mock mongo services
        $mongoService = Mockery::mock('MongoService');
        // $mongoService->shouldReceive('addIdTodata')->andReturn([[
        //         "tx_ref_id"     => "655ab2c",
        //         "brand_id"      => "801acb59",
        //         "outlet_id"     => "6900006",
        //         "terminal_id"   => "87ab6e2f",
        //         "account_type"  => "merchant",
        //         "account_value" => "20",
        //         "reward_code"   => "",
        //         "campaign_code" => "9ba2be3",
        //         "id"            => "590008bb1d2dc901014049c9"
        // ]]);
        $mongoService->shouldReceive('manageSortDataByIdList')->andReturn([[
                "tx_ref_id"     => "655ab2c",
                "brand_id"      => "801acb59",
                "outlet_id"     => "6900006",
                "terminal_id"   => "87ab6e2f",
                "account_type"  => "merchant",
                "account_value" => "20",
                "campaign_code" => "9ba2be3",
                "reward_code"   => "",
                "id"            => "590008bb1d2dc901014049c9"
        ]]);

        //register model
        $this->di->set('mongoService', $mongoService, true);

        //create class
        $repo = $this->getMockBuilder('App\Repositories\RedeemRepository')
                           ->setMethods([
                                'getDetailDataByIdLargeData'
                            ])
                           ->getMock();
                           
        $repo->method('getDetailDataByIdLargeData')
                   ->willReturn($tag);

        //call method 
        $result = $repo->getTransactionDetail(['id' => '590008bb1d2dc901014049c9']);

        //check result
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('success', $result);
        $this->assertArrayHasKey('message', $result);
        $this->assertTrue($result['success']);
        $this->assertArrayHasKey('data', $result);
        $this->assertInternalType('array', $result['data']);
        $this->assertArrayHasKey('id', $result['data'][0]);
        $this->assertArrayHasKey('tx_ref_id', $result['data'][0]);
        $this->assertArrayHasKey('brand_id', $result['data'][0]);
        $this->assertArrayHasKey('outlet_id', $result['data'][0]);
        $this->assertArrayHasKey('terminal_id', $result['data'][0]);
        $this->assertArrayHasKey('account_type', $result['data'][0]);
        $this->assertArrayHasKey('account_value', $result['data'][0]);
        $this->assertArrayHasKey('campaign_code', $result['data'][0]);
        $this->assertArrayHasKey('reward_code', $result['data'][0]);

    }

    public function testRedeemIfRollBackMissionFail()
    {
        //create class
        $repo = new RedeemRepository();

        //call method 
         $result = $this->callMethod(
            $repo,
            'redeem',
            [['id' => '8bd97ec5a'],'rollback']
        );

        //check result
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('success', $result);
        $this->assertArrayHasKey('message', $result);
        $this->assertFalse($result['success']);
        $this->assertEquals('missionFail', $result['message']);
    }

    public function testRedeemIfRollBackSuccessResTrue()
    {
        //default
        $params = [
            [
                "tx_ref_id"     => "655ab2c",
                "brand_id"      => "801acb59",
                "outlet_id"     => "6900006",
                "terminal_id"   => "87ab6e2f",
                "acc_type"      => "merchant",
                "acc_value"     => "20",
                "campaign_code" => "9ba2be3",
                "reward_code"   => "",                
            ],
            "status"        => "fail",
            "action"        => "sale",
            'id'            => '8bd97ec5a'
        ];

        $service = Mockery::mock('Service'); 
        $service->shouldReceive('redeem')->andReturn([ 
                                            'success' => true, 
                                            'message' => '', 
                                                'data' => [ 
                                                    'id'          => '123',
                                                    'description' => 'test'
                                                        ],      
                                            ]); 

        //create repo
        $repo = $this->getMockBuilder('App\Repositories\RedeemRepository')
                           ->setMethods([
                                'getServiceClass',
                                'xmlFormat',
                                'updateStatusRedeem'
                                ])
                           ->getMock();
        
        $repo->method('getServiceClass')->willReturn($service);

        $repo->method('xmlFormat')->willReturn('Test');

        
        $repo->method('updateStatusRedeem')->willReturn([
                                                    'success' => true,
                                                    'message' => '',
                                                    'data'    => 'test'
                                                    ]);
        
        //call method 
         $result = $this->callMethod(
            $repo,
            'redeem',
            [$params,'rollback']
        );
        

        //check result
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('success', $result);
        $this->assertArrayHasKey('message', $result);
        $this->assertArrayHasKey('data', $result);
        $this->assertInternalType('array', $result['data']);
        
    }

    public function testRedeemIfRollBackSuccessResFalse()
    {
        //default
        $params = [
            [
                "tx_ref_id"     => "655ab2c",
                "brand_id"      => "801acb59",
                "outlet_id"     => "6900006",
                "terminal_id"   => "87ab6e2f",
                "acc_type"      => "merchant",
                "acc_value"     => "20",
                "campaign_code" => "9ba2be3",
                "reward_code"   => "",                
            ],
            "status"        => "fail",
            "action"        => "sale",
            'id'            => '8bd97ec5a'
        ];

        $service = Mockery::mock('Service'); 
        $service->shouldReceive('redeem')->andReturn([ 
                                            'success' => false, 
                                            'message' => '', 
                                                'data' => [ 
                                                    'id'          => '123',
                                                    'description' => 'test'
                                                        ],      
                                            ]); 

        //create repo
        $repo = $this->getMockBuilder('App\Repositories\RedeemRepository')
                           ->setMethods([
                                'getServiceClass',
                                'xmlFormat',
                                'updateStatusRedeem'
                                ])
                           ->getMock();
        
        $repo->method('getServiceClass')->willReturn($service);

        $repo->method('xmlFormat')->willReturn('Test');

        
        $repo->method('updateStatusRedeem')->willReturn([
                                                    'success' => true,
                                                    'message' => '',
                                                    'data'    => 'test'
                                                    ]);
        
        //call method 
         $result = $this->callMethod(
            $repo,
            'redeem',
            [$params,'rollback']
        );
        

        //check result
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('success', $result);
        $this->assertArrayHasKey('message', $result);
        $this->assertArrayHasKey('data', $result);
        $this->assertInternalType('array', $result['data']);
        
    }

    public function testRedeemIfNotRollBackSuccess()
    {
        //default
        $params = [
            [
                "tx_ref_id"     => "655ab2c",
                "brand_id"      => "801acb59",
                "outlet_id"     => "6900006",
                "terminal_id"   => "87ab6e2f",
                "acc_type"      => "merchant",
                "acc_value"     => "20",
                "campaign_code" => "9ba2be3",
                "reward_code"   => "",                
            ],
            "status"        => "waiting",
            "action"        => "mark_use"
        ];
        //mock user
        $redeem  = Mockery::mock('Redeem');
        $redeem->_id = '8bd97ec5a';
        //mock model
        $model = Mockery::mock('Model');
        //Mock service
        $service = Mockery::mock('Service'); 
        $service->shouldReceive('redeem')->andReturn([ 
                                            'success' => false, 
                                            'message' => '', 
                                                'data' => [ 
                                                    'id'          => '123',
                                                    'description' => 'test'
                                                        ],      
                                            ]);
        //create repo
        $repo = $this->getMockBuilder('App\Repositories\RedeemRepository')
                           ->setMethods([
                                'getRedeemModel',
                                'insertRedeem',
                                'getServiceClass',
                                'xmlFormat',
                                'updateStatusRedeem'                                
                                ])
                           ->getMock();
        $repo->method('getRedeemModel')->willReturn($model);

        $repo->method('insertRedeem')->willReturn($redeem);

        $repo->method('getServiceClass')->willReturn($service);

        $repo->method('xmlFormat')->willReturn('Test');

        
        $repo->method('updateStatusRedeem')->willReturn([
                                                    'success' => true,
                                                    'message' => '',
                                                    'data'    => 'test'
                                                    ]);
        
        //call method 
         $result = $this->callMethod(
            $repo,
            'redeem',
            [$params,'mark_use']
        );

        //check result
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('success', $result);
        $this->assertArrayHasKey('message', $result);
        $this->assertArrayHasKey('data', $result);
        $this->assertInternalType('array', $result['data']);
        
    }
            
    public function testRedeemByCampaign()
    {
        //default params
        $params = [
            [
                "tx_ref_id"     => "655ab2c",
                "brand_id"      => "801acb59",
                "outlet_id"     => "6900006",
                "terminal_id"   => "87ab6e2f",
                "acc_type"      => "merchant",
                "acc_value"     => "20",
                "campaign_code" => "9ba2be3",
                "reward_code"   => "",
                
            ],
            "status"        => "merchant",
            "action"        => "sale"
        ];

        $ansMockForThisMethod = [
        "success" => 1,
        "message" => "Test",
        "data" => 
            [
                "code" => "722",
                "description" => "not found terminal_id",
                "ext_transaction_id" => "87ab6e2f",
                "service_desc" => "Truecard::request_benefit",
                "method" => "request_benefit",
                "channel" => "RPP",
                "transaction_id" => "7181020391854000f17dw-stagea01RPP87ab6e2f",
                "point_balance" => 0,
                "moreinfo" => 
                    [
                        "message" => "not found terminal_id",
                    ],

                "body" => 
                    [
                        "items" => 
                            [
                                "0" => "Test",
                            ],

                    ],

            ],
        ];

        //create class
        $repo = $this->getMockBuilder('App\Repositories\RedeemRepository')
                           ->setMethods([
                                'redeem'
                            ])
                           ->getMock();

        $repo->method('redeem')->willReturn($ansMockForThisMethod);

        //call method 
        $result = $this->callMethod(
            $repo,
            'redeemByCampaign',
            [$params]
        );
        //check result
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('success', $result);
        $this->assertArrayHasKey('message', $result);
        $this->assertArrayHasKey('data', $result);
        $this->assertInternalType('array', $result['data']);
        $this->assertEquals($ansMockForThisMethod, $result);

    }

    public function testRedeemByCode()
    {
        //default params
        $params = [
            [
                "tx_ref_id"     => "655ab2c",
                "brand_id"      => "801acb59",
                "outlet_id"     => "6900006",
                "terminal_id"   => "87ab6e2f",
                "acc_type"      => "merchant",
                "acc_value"     => "20",
                "campaign_code" => "29ab35d",
                "reward_code"   => "9ba2be3",
                
            ],
            "status"        => "merchant",
            "action"        => "sale"
        ];

        $ansMockForThisMethod = [
        "success" => 1,
        "message" => "Test",
        "data" => 
            [
                "code" => "722",
                "description" => "not found terminal_id",
                "ext_transaction_id" => "87ab6e2f",
                "service_desc" => "Truecard::mark_use",
                "method" => "mark_use",
                "channel" => "RPP",
                "transaction_id" => "7181020391854000f17dw-stagea01RPP87ab6e2f",
                "point_balance" => 0,
                "moreinfo" => 
                    [
                        "message" => "not found terminal_id",
                    ],

                "body" => 
                    [
                        "items" => 
                            [
                                "0" => "Test",
                            ],

                    ],

            ],
        ];

        //create class
        $repo = $this->getMockBuilder('App\Repositories\RedeemRepository')
                           ->setMethods([
                                'redeem'
                            ])
                           ->getMock();

        $repo->method('redeem')->willReturn($ansMockForThisMethod);

        //call method 
        $result = $this->callMethod(
            $repo,
            'redeemByCode',
            [$params]
        );
        //check result
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('success', $result);
        $this->assertArrayHasKey('message', $result);
        $this->assertArrayHasKey('data', $result);
        $this->assertInternalType('array', $result['data']);
        $this->assertEquals($ansMockForThisMethod, $result);

    }

    public function testRedeemReversalEmptyTransactionData()
    {
        $params = [
            [
                "tx_ref_id"     => "655ab2c",
                "brand_id"      => "801acb59",
                "outlet_id"     => "6900006",
                "terminal_id"   => "87ab6e2f",
                "transaction_id"=> "7181020391854000f17dw-stagea01RPP87ab6e2f",
                "acc_type"      => "merchant",
                "acc_value"     => "20",
                "campaign_code" => "29ab35d",
                "reward_code"   => "9ba2be3",
                
            ],
            "status"        => "merchant",
            "action"        => "sale"
        ];

        //create class
        $repo = $this->getMockBuilder('App\Repositories\RedeemRepository')
                           ->setMethods([
                                'filterSearchDataTransactions',
                                'getTransaction'
                            ])
                           ->getMock();

        $repo->method('filterSearchDataTransactions')->willReturn([
                                                                "tx_ref_id" => "655ab2c",
                                                                "brand_id"  => "801acb59"
                                                        ]);
        $repo->method('getTransaction')->willReturn([
                                            "success" => 1,
                                            "message" => '',
                                            "data" => []
                                            ]);

        //call method 
        $result = $this->callMethod(
            $repo,
            'redeemReversal',
            [$params]
        );

        //check result
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('success', $result);
        $this->assertArrayHasKey('message', $result);
        $this->assertArrayHasKey('data', $result);
        
        
    }

    public function testRedeemReversalStatusTypeEqualsSuccess()
    {
        $params = [
            [
                "tx_ref_id"     => "655ab2c",
                "brand_id"      => "801acb59",
                "outlet_id"     => "6900006",
                "terminal_id"   => "87ab6e2f",
                "transaction_id"=> "7181020391854000f17dw-stagea01RPP87ab6e2f",
                "acc_type"      => "merchant",
                "acc_value"     => "20",
                "campaign_code" => "29ab35d",
                "reward_code"   => "9ba2be3"
                
            ],
            "status"        => "success",
            "action"        => "REVERSAL"
        ];

        $mockDetailData = 

        //create class
        $repo = $this->getMockBuilder('App\Repositories\RedeemRepository')
                           ->setMethods([
                                'filterSearchDataTransactions',
                                'getTransaction',
                                'getTransactionDetail'
                            ])
                           ->getMock();

        $repo->method('filterSearchDataTransactions')->willReturn([
                                                                "tx_ref_id" => "655ab2c",
                                                                "brand_id"  => "801acb59"
                                                        ]);
        $repo->method('getTransaction')->willReturn([
                "success" => 1,
                "message" => '',
                "data" => "590008bb1d2dc901014049c9"
                ]);

        $repo->method('getTransactionDetail')->willReturn([
                "data"   => [
                        [
                            "action"            => "REVERSAL",
                            "status"            => "success",
                            "id"                => "590008bb1d2dc901014049c9",
                            "ext_ref_id"        => "655ab2c",
                            "account_type"      => "merchant",
                            "account_value"     => "20",
                            "ext_response_data" => [
                                "tx_ref_id"     => "655ab2c",
                                "brand_id"      => "801acb59",
                                "outlet_id"     => "6900006",
                                "terminal_id"   => "87ab6e2f",
                                "transaction_id"=> "7181020391854000f17dw-stagea01RPP87ab6e2f",
                                "acc_type"      => "merchant",
                                "acc_value"     => "20",
                                "campaign_code" => "29ab35d",
                                "reward_code"   => "9ba2be3"
                                ]
                            ]
                        ]
                ]);
        
        //call method 
        $result = $this->callMethod(
            $repo,
            'redeemReversal',
            [$params]
        );
           

        //check result
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('data', $result);
        $this->assertInternalType('array', $result['data']);
        
    }

    public function testRedeemReversalActionNotReversal()
    {
        $params = [
            [
                "tx_ref_id"     => "655ab2c",
                "brand_id"      => "801acb59",
                "outlet_id"     => "6900006",
                "terminal_id"   => "87ab6e2f",
                "transaction_id"=> "7181020391854000f17dw-stagea01RPP87ab6e2f",
                "acc_type"      => "merchant",
                "acc_value"     => "20",
                "campaign_code" => "29ab35d",
                "reward_code"   => "9ba2be3",
                
            ],
            "status"        => "REVERSAL",
            "action"        => "fail"
        ];

        $mockDetailData = 

        //create class
        $repo = $this->getMockBuilder('App\Repositories\RedeemRepository')
                           ->setMethods([
                                'filterSearchDataTransactions',
                                'getTransaction',
                                'getTransactionDetail'
                            ])
                           ->getMock();

        $repo->method('filterSearchDataTransactions')->willReturn([
                                                                "tx_ref_id" => "655ab2c",
                                                                "brand_id"  => "801acb59"
                                                        ]);
        $repo->method('getTransaction')->willReturn([
                                            "success" => 1,
                                            "message" => '',
                                            "data" => "590008bb1d2dc901014049c9"
                                            ]);
        $repo->method('getTransactionDetail')->willReturn([
                                                    "data"   => [
                                                            [
                                                                "action"        => "REVERSAL",
                                                                "status"        => "fail",
                                                                "id"            => "590008bb1d2dc901014049c9",
                                                                "ext_ref_id"    => "655ab2c",
                                                                "account_type"  => "merchant",
                                                                "campaign_code" => "29ab35d",
                                                                "account_value" => "20"
                                                                ]
                                                            ]
                                                    ]);

        
        //call method 
        $result = $this->callMethod(
            $repo,
            'redeemReversal',
            [$params]
        );

        //check result
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('data', $result);
        $this->assertInternalType('array', $result['data']);

        
    }

    public function testRedeemReversalStatusTypeEqualsFail()
    {
        $params = [
            [
                "tx_ref_id"     => "655ab2c",
                "brand_id"      => "801acb59",
                "outlet_id"     => "6900006",
                "terminal_id"   => "87ab6e2f",
                "transaction_id"=> "7181020391854000f17dw-stagea01RPP87ab6e2f",
                "acc_type"      => "merchant",
                "acc_value"     => "20",
                "campaign_code" => "29ab35d",
                "reward_code"   => "9ba2be3",
                
            ],
            "status"        => "rollback",
            "action"        => "fail"
        ];

        $mockDetailData = 

        //create class
        $repo = $this->getMockBuilder('App\Repositories\RedeemRepository')
                           ->setMethods([
                                'filterSearchDataTransactions',
                                'getTransaction',
                                'getTransactionDetail'
                            ])
                           ->getMock();

        $repo->method('filterSearchDataTransactions')->willReturn([
                                                                "tx_ref_id" => "655ab2c",
                                                                "brand_id"  => "801acb59"
                                                        ]);
        $repo->method('getTransaction')->willReturn([
                                            "success" => 1,
                                            "message" => '',
                                            "data" => "590008bb1d2dc901014049c9"
                                            ]);
        $repo->method('getTransactionDetail')->willReturn([
                                                    "data"   => [
                                                            [
                                                                "action"        => "rollback",
                                                                "status"        => "fail",
                                                                "id"            => "590008bb1d2dc901014049c9",
                                                                "ext_ref_id"    => "655ab2c",
                                                                "account_type"  => "merchant",
                                                                "account_value" => "20",
                                                                "campaign_code" => "29ab35d"
                                                                ]
                                                            ]
                                                    ]);

        
        //call method 
        $result = $this->callMethod(
            $repo,
            'redeemReversal',
            [$params]
        );

        //check result
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('data', $result);
        $this->assertInternalType('array', $result['data']);

        
    }

    public function testRedeemReversalSuccess()
    {
        $params = [
            [
                "tx_ref_id"     => "655ab2c",
                "brand_id"      => "801acb59",
                "outlet_id"     => "6900006",
                "terminal_id"   => "87ab6e2f",
                "transaction_id"=> "7181020391854000f17dw-stagea01RPP87ab6e2f",
                "acc_type"      => "merchant",
                "acc_value"     => "20",
                "campaign_code" => "29ab35d",
                "reward_code"   => "9ba2be3",
                
            ],
            "status"        => "rollback",
            "action"        => "rollback"
        ];

        $mockDetailData = 

        //create class
        $repo = $this->getMockBuilder('App\Repositories\RedeemRepository')
                           ->setMethods([
                                'filterSearchDataTransactions',
                                'getTransaction',
                                'getTransactionDetail',
                                'redeem'
                            ])
                           ->getMock();

        $repo->method('filterSearchDataTransactions')->willReturn([
                                                                "tx_ref_id" => "655ab2c",
                                                                "brand_id"  => "801acb59"
                                                        ]);
        $repo->method('getTransaction')->willReturn([
                                            "success" => 1,
                                            "message" => '',
                                            "data" => "590008bb1d2dc901014049c9"
                                            ]);
        $repo->method('getTransactionDetail')->willReturn([
                                                    "data"   => [
                                                            [
                                                                "action"        => "rollback",
                                                                "status"        => "rollback",
                                                                "id"            => "590008bb1d2dc901014049c9",
                                                                "ext_ref_id"    => "655ab2c",
                                                                "account_type"  => "merchant",
                                                                "account_value" => "20",
                                                                "campaign_code" => "29ab35d"
                                                                ]
                                                            ]
                                                    ]);

        $repo->method('redeem')->willReturn([
                                            'success' => true,
                                            'message' => '',
                                            'data'    => [
                                                        "action"        => "rollback",
                                                        "status"        => "rollback",
                                                        "id"            => "590008bb1d2dc901014049c9",
                                                        "ext_ref_id"    => "655ab2c",
                                                        "account_type"  => "merchant",
                                                        "account_value" => "20"
                                                        ],
                                            ]);
        
        //call method 
        $result = $this->callMethod(
            $repo,
            'redeemReversal',
            [$params]
        );
        

        //check result
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('success', $result);
        $this->assertArrayHasKey('message', $result);
        $this->assertArrayHasKey('data', $result);
        $this->assertInternalType('array', $result['data']);

        
    }

    public function testFilterSearchDataTransactions()
    {
        //default params
        $params = [
                "tx_ref_id"     => "655ab2c",
                "brand_id"      => "801acb59",
                "outlet_id"     => "6900006",
                "terminal_id"   => "87ab6e2f",
                "acc_type"      => "merchant",
                "acc_value"     => "20",
                "campaign_code" => "29ab35d",
                "reward_code"   => "9ba2be3",
            ];
        //create class
        $repo = new RedeemRepository();

        //call method 
        $result = $this->callMethod(
            $repo,
            'filterSearchDataTransactions',
            [$params]
        );
        
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('tx_ref_id', $result);
        $this->assertArrayHasKey('brand_id', $result);
        $this->assertEquals('655ab2c', $result['tx_ref_id']);
        $this->assertEquals('801acb59', $result['brand_id']);
        
    }

    public function testGetRedeemException()
    {
        //create class
        $repo = new RedeemRepository();

        //call method 
        $result = $repo->getRedeem(['id_only' => true]);

        //check result
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('success', $result);
        $this->assertArrayHasKey('message', $result);
        $this->assertFalse($result['success']);
        $this->assertEquals('missionFail', $result['message']);

    }


     public function testGetRedeemSuccessNoLimit()
    {
        //Mock type
        $redeem = Mockery::mock('Redeem');

        

        //Mock mongo services
        $mongoService = Mockery::mock('MongoService');
        $mongoService->shouldReceive('manageBetweenFilter')->andReturn([
                ['ext_response_date' => ['2017-06-09 00:00:00','2017-06-09 23:59:59']],
                ['ext_response_date' => ['$gte','$lte']]
        ]);

        $mongoService->shouldReceive('getAllIdFromDatas')->andReturn('58abd2f22f8331000a3acb92');


        //register model
        $this->di->set('mongoService', $mongoService, true);

        //creste class
        $repo = $this->getMockBuilder('App\Repositories\RedeemRepository')
                    ->setMethods(['getDataByParams'])
                    ->getMock();


        $repo->method('getDataByParams')
            ->willReturn([$redeem, 1]);

        //call method 
        $result = $repo->getRedeem(['id_only' => true]);

        //check result
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('success', $result);
        $this->assertArrayHasKey('message', $result);
        $this->assertTrue($result['success']);
        $this->assertArrayHasKey('data', $result);
        $this->assertInternalType('string', $result['data']);
        $this->assertEquals('58abd2f22f8331000a3acb92', $result['data']);

    }

    public function testGetRedeemSuccessWithData()
    {
        //Mock type
        $redeem = Mockery::mock('Redeem');

       

        //Mock mongo services
        $mongoService = Mockery::mock('MongoService');

         //register model
        $this->di->set('mongoService', $mongoService, true);

        $mongoService->shouldReceive('addIdTodata')->andReturn([
                [
                    
                    "id"        => "58abd2f22f8331000a3acb92",
                    "limit"     => "2" ,
                ]
            ]);

         $mongoService->shouldReceive('manageBetweenFilter')->andReturn(
                ['ext_response_date' => ['2017-06-09 00:00:00','2017-06-09 23:59:59']],
                ['ext_response_date' => ['$gte','$lte']]
        );

        
        //creste class
        $repo = $this->getMockBuilder('App\Repositories\RedeemRepository')
                    ->setMethods(['getDataByParams'])
                    ->getMock();

        $repo->method('getDataByParams')
            ->willReturn([$redeem, 1]);

        //call method 
        $result = $repo->getRedeem([]);

        //check result
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('success', $result);
        $this->assertArrayHasKey('message', $result);
        $this->assertTrue($result['success']);
        $this->assertArrayHasKey('data', $result);
        $this->assertInternalType('array', $result['data']);
        $this->assertArrayHasKey('id', $result['data'][0]);
        $this->assertArrayHasKey('limit', $result['data'][0]);
       

    }


    public function testGetRedeemSuccessHaveLimit()
    {
        //Mock type
        $redeem = Mockery::mock('Redeem');

        //Mock mongo services
        $mongoService = Mockery::mock('MongoService');

         //register model
        $this->di->set('mongoService', $mongoService, true);

        $mongoService->shouldReceive('manageBetweenFilter')->andReturn(
                ['ext_response_date' => ['2017-06-09 00:00:00','2017-06-09 23:59:59']],
                ['ext_response_date' => ['$gte','$lte']]
        );
        $mongoService->shouldReceive('getAllIdFromDatas')->andReturn('58abd2f22f8331000a3acb92');

        
        //creste class
        $repo = $this->getMockBuilder('App\Repositories\RedeemRepository')
                    ->setMethods(['getDataByParams'])
                    ->getMock();

        $repo->method('getDataByParams')
            ->willReturn([$redeem, 1]);

        //call method 
        $result = $repo->getRedeem(['id_only' => true, 'limit' => 2]);

        //check result
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('success', $result);
        $this->assertArrayHasKey('message', $result);
        $this->assertTrue($result['success']);
        $this->assertArrayHasKey('data', $result);
        $this->assertInternalType('string', $result['data']);
        $this->assertEquals('58abd2f22f8331000a3acb92', $result['data']);
        $this->assertArrayHasKey('totalRecord', $result);
        $this->assertEquals(1, $result['totalRecord']);

    }

    public function testInsertDataError()
    {
        //mock model
        $model        = Mockery::mock('Model');
        $model->title = null;
        $model->point = null;
        $model->cash  = null;

        $model->shouldReceive("save")->andReturn(false);

        //create class
        $repo = new RedeemRepository();

        //create params
        $params = [$model, [
            'title' => [
                'en' => 'Test',
                'th' => 'ทดสอบ'
            ],
            'xxx'   => 'xxx'
        ]];

        //call method
        $result = $this->callMethod(
            $repo,
            'insertData',
            $params
        );

        //check result
        $this->assertNull($result);
    }

    public function testInsertDataSuccess()
    {
        //mock model
        $model        = Mockery::mock('Model');
        $model->title = null;
        $model->point = null;
        $model->cash  = null;
        $model->shouldReceive("save")->andReturn(true);
        $model->shouldReceive('getOnlyData')->andReturn($model);

        //create class
        $repo = new RedeemRepository();


        //create params
        $params = [$model, [
            'tx_ref_id'   => '1234'
        ]];

        //call method
        $result = $this->callMethod(
            $repo,
            'insertData',
            $params
        ); 

        //check result
        $this->assertInternalType('object', $result);
        //$this->assertArrayHasKey('1234', $result->tx_ref_id);

    }


     public function testUpdateDataError()
    {
        //mock category
        $deal        = Mockery::mock('Deal');
        $deal->_id   = '58e27db58d6a71405dbbdb32';
        $deal->title = null;
        $deal->shouldReceive('save')->andReturn(false);

        //create params
        $params = [$deal, [
            'title' => [
                'en' => 'Test',
                'th' => 'ทดสอบ'
            ],
            'xxx'   => 'xxx'
        ]];

        //create class
        $repo = new RedeemRepository();

        //call method
        $result = $this->callMethod(
            $repo,
            'updateData',
            $params
        ); 

        //check result
        $this->assertNull($result);
    }

    public function testUpdateDataSuccess()
    {
        //mock category
        $deal        = Mockery::mock('Deal');
        $deal->_id   = '58e27db58d6a71405dbbdb32';
        $deal->title = null;
        $deal->point = null;
        $deal->cash  = null;
        $deal->shouldReceive('save')->andReturn(true);
        $deal->shouldReceive('getOnlyData')->andReturn($deal);

        //create params
        $params = [$deal, [
            'title' => [
                'en' => 'Test',
                'th' => 'ทดสอบ'
            ],
            'xxx'   => 'xxx'
        ]];

        //create class
        $repo = new RedeemRepository();

        //call method
        $result = $this->callMethod(
            $repo,
            'updateData',
            $params
        ); 

        //check result
        $this->assertInternalType('object', $result);
        $this->assertInternalType('array', $result->title);
        $this->assertArrayHasKey('en', $result->title);
        $this->assertArrayHasKey('th', $result->title);
        $this->assertEquals('58e27db58d6a71405dbbdb32', $result->_id);
    }



    public function testGetRedeemByIdNoData()
    {
        //mock category model
        $dealModel = Mockery::mock('Redeem');
        $dealModel->shouldReceive('findById')->andReturn(null);

        //create class
        $repo = $this->getMockBuilder('App\Repositories\RedeemRepository')
                           ->setMethods([
                                'getRedeemModel'
                            ])
                           ->getMock();
                           
        $repo->method('getRedeemModel')
                   ->willReturn($dealModel);


        //call method

        $result = $this->callMethod(
            $repo,
            'getRedeemById',
            ['99e27db58d6a71405dbbdb32']
        ); 

        //check result
        $this->assertNull($result);

    }

    public function testGetRedeemByIdHaveData()
    {
        //Mock data
        $data               = Mockery::mock('Data');
        $data->_id          = '99e27db58d6a71405dbbdb32';
        $data->content_type = 'merchant';
        $data->content_id   = '001';
        //mock category model
        $dealModel = Mockery::mock('Redeem');
        $dealModel->shouldReceive('findById')->andReturn($data);

        //create class
        $repo = $this->getMockBuilder('App\Repositories\RedeemRepository')
                           ->setMethods([
                                'getRedeemModel'
                            ])
                           ->getMock();
                           
        $repo->method('getRedeemModel')
                   ->willReturn($dealModel);


        //call method

        $result = $this->callMethod(
            $repo,
            'getRedeemById',
            ['99e27db58d6a71405dbbdb32']
        ); 

        
        //check result
        $this->assertNotNull($result);
        $this->assertInternalType('object', $result);
        $this->assertEquals('99e27db58d6a71405dbbdb32', $result->_id);

    }


    public function testGetDetailDataByIdLargeData()
    {
        //Mock user
        $model = Mockery::mock('Redeem');

        //Mock mongoService
        $mongoService = Mockery::mock('MongoService');
        $mongoService->shouldReceive('getDetailDataByIdLargeData')->andReturn($model);

        //register repository
        $this->di->set('mongoService', $mongoService, true);

        //create repo
        $repo = new RedeemRepository();
        
        //create params
        $params = ['58e27db58d6a71405dbbdb32','redeem'];

        //call method
        $result = $this->callMethod(
            $repo,
            'getDetailDataByIdLargeData',
            $params
        );

        //check result
        $this->assertInternalType('object', $result);
        $this->assertNotNull($result);
    }





}
?>