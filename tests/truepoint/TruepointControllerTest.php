<?php

use App\Controllers\TruepointController;
use Phalcon\DI;

class TruepointControllerTest extends UnitTestCase
{
    //------ start: MOCK DATA ---------//
    private $getDetailInputs = [
        'id' => '58eb5ab69aaf84001429e402,58eb5bc89aaf840010437512'
    ];

    //------ end: MOCK DATA ---------//

    //------- start: Method for support test --------//
    protected static function callMethod($obj, $name, array $args)
    {
        $class  = new \ReflectionClass($obj);
        $method = $class->getMethod($name);
        $method->setAccessible(true);

        return $method->invokeArgs($obj, $args);
    }
    //------- end: Method for support test --------//

    //------- start: Test function ---------//

    public function testGetTruepointRepository()
    {
        //mock repository
        $repository = Mockery::mock('Repository');
        $repository->shouldReceive("getRepository")->andReturn("TEST");

        //register
        $this->di->set('repository', $repository, true);

        //create class
        $user = new TruepointController();
        
        //call method
        $result   = $this->callMethod(
                $user,
                'getTruepointRepository',
               []
            );

        //check result
        $this->assertEquals( $result, "TEST" );
    }


    public function testPostBurnPointActionValidateError(){
         //create class
        $user = $this->getMockBuilder('App\Controllers\TruepointController')
                           ->setMethods([
                                'getPostInput',
                                'validateApi',
                                'validateError'])
                           ->getMock();

        $user->method('getPostInput')
                   ->willReturn([]);

        $user->method('validateApi')
                   ->willReturn([
                        'msgError'   => 'The customer_id is required',
                        'fieldError' => 'customer_id'
                    ]);

        $user->method('validateError')
                   ->willReturn([
                        'status' => [
                            'code'    => 400,
                            'message' => 'Bad Request',
                        ],
                        'error' => [
                            'message'  => 'The customer_id is required',
                            "property" => "customer_id"
                        ]
                    ]);

        //call method
        $result = $user->postBurnPointAction();

        //check result
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('status', $result);
        $this->assertInternalType('array', $result['status']);
        $this->assertArrayHasKey('code', $result['status']);
        $this->assertArrayHasKey('message', $result['status']);
        $this->assertEquals($result['status']['code'], 400);
        $this->assertEquals($result['status']['message'], 'Bad Request');
        $this->assertArrayHasKey('error', $result);
        $this->assertInternalType('array', $result['error']);
        $this->assertArrayHasKey('message', $result['error']);
        $this->assertEquals($result['error']['message'], 'The customer_id is required');
        $this->assertArrayHasKey('property', $result['error']);
        $this->assertEquals($result['error']['property'], 'customer_id');

    }


    public function testPostBurnPointActionError(){
         //create class
        $burnpointRepo = Mockery::mock('TruepointRepository');
        $burnpointRepo->shouldReceive("burnPoint")->andReturn([
            'success' => false,
            'message' => 'burnError',
        ]);
        //create class
        $user = $this->getMockBuilder('App\Controllers\TruepointController')
                           ->setMethods([
                                'getPostInput',
                                'validateApi',
                                'getTruepointRepository',
                                'validateBussinessError'
                            ])
                           ->getMock();

        $user->method('getPostInput')
                   ->willReturn([]);

         $user->method('validateApi')
                   ->willReturn(['']);

        $user->method('getTruepointRepository')
                   ->willReturn($burnpointRepo);


        $user->method('validateBussinessError')
                   ->willReturn([
                        'status' => [
                            'code'    => 400,
                            'message' => 'Bad request',
                        ],
                        'error' => [
                            "message" =>  "Cannot earn point"
                        ]
                    ]);

        //call method
        $result = $user->postBurnPointAction();

        //check result
        $this->assertInternalType('array', $result);
        $this->assertEquals($result['status']['code'], 400);
        $this->assertEquals($result['status']['message'], 'Bad request');
        $this->assertArrayHasKey('error', $result);
        $this->assertArrayHasKey('message', $result['error']);
        $this->assertEquals($result['error']['message'], 'Cannot earn point');

    }

    public function testPostBurnPointActionSuccess()
    {
        $cacheService = Mockery::mock('CacheService');
        $cacheService->shouldReceive('deleteCacheByPrefix')->andReturn(null);
        //cache
        $this->di->set('cacheService', $cacheService, true);

         //create class
        $burnpointRepo = Mockery::mock('TruepointRepository');
        $burnpointRepo->shouldReceive("burnPoint")->andReturn([
            'success' => true,
            'message' => '',
            'data'  => [
                "customer_id"  => "7",
                "content_type" => "couponw",
                "content_id"   => "1",
                "point"        => 50,
                "status"       => "success",
                "type"         => "burn",
                "used_date"    => "2017-04-26 09:40:59",
                "id"           => "590008bb1d2dc901014049c9"
            ]
        ]);
        //create class
        $user = $this->getMockBuilder('App\Controllers\TruepointController')
                           ->setMethods([
                                'getPostInput',
                                'validateApi',
                                'getTruepointRepository',
                                'output'
                            ])
                           ->getMock();

        $user->method('getPostInput')
                   ->willReturn([]);

         $user->method('validateApi')
                   ->willReturn(['']);

        $user->method('getTruepointRepository')
                   ->willReturn($burnpointRepo);


        $user->method('output')
                   ->willReturn([
                        'status' => [
                            'code'    => 200,
                            'message' => 'Success',
                        ],
                        'data'    => [
                            "customer_id"  => "7",
                            "content_type" => "couponw",
                            "content_id"   => "1",
                            "point"        => 50,
                            "status"       => "success",
                            "type"         => "burn",
                            "used_date"    => "2017-04-26 09:40:59",
                            "id"           => "590008bb1d2dc901014049c9"
                        ]
                    ]);

        //call method
        $result = $user->postBurnPointAction();

        //check result
        $this->assertInternalType('array', $result);
        $this->assertEquals($result['status']['code'], 200);
        $this->assertEquals($result['status']['message'], 'Success');
        $this->assertArrayHasKey('data', $result);

    }

    public function testPostEarnPointActionValidateError(){
         //create class
        $user = $this->getMockBuilder('App\Controllers\TruepointController')
                           ->setMethods([
                                'getPostInput',
                                'validateApi',
                                'validateError'])
                           ->getMock();

        $user->method('getPostInput')
                   ->willReturn([]);

        $user->method('validateApi')
                   ->willReturn([
                        'msgError'   => 'The customer_id is required',
                        'fieldError' => 'customer_id'
                    ]);

        $user->method('validateError')
                   ->willReturn([
                        'status' => [
                            'code'    => 400,
                            'message' => 'Bad Request',
                        ],
                        'error' => [
                            'message'  => 'The customer_id is required',
                            "property" => "customer_id"
                        ]
                    ]);

        //call method
        $result = $user->postEarnPointAction();

        //check result
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('status', $result);
        $this->assertInternalType('array', $result['status']);
        $this->assertArrayHasKey('code', $result['status']);
        $this->assertArrayHasKey('message', $result['status']);
        $this->assertEquals($result['status']['code'], 400);
        $this->assertEquals($result['status']['message'], 'Bad Request');
        $this->assertArrayHasKey('error', $result);
        $this->assertInternalType('array', $result['error']);
        $this->assertArrayHasKey('message', $result['error']);
        $this->assertEquals($result['error']['message'], 'The customer_id is required');
        $this->assertArrayHasKey('property', $result['error']);
        $this->assertEquals($result['error']['property'], 'customer_id');

    }

     public function testPostEarnPointActionError(){
         //create class
        $earnpointRepo = Mockery::mock('TruepointRepository');
        $earnpointRepo->shouldReceive("earnPoint")->andReturn([
            'success' => false,
            'message' => 'earnError',
        ]);
        //create class
        $user = $this->getMockBuilder('App\Controllers\TruepointController')
                           ->setMethods([
                                'getPostInput',
                                'validateApi',
                                'getTruepointRepository',
                                'validateBussinessError'
                            ])
                           ->getMock();

        $user->method('getPostInput')
                   ->willReturn([]);

         $user->method('validateApi')
                   ->willReturn(['']);

        $user->method('getTruepointRepository')
                   ->willReturn($earnpointRepo);


        $user->method('validateBussinessError')
                   ->willReturn([
                        'status' => [
                            'code'    => 400,
                            'message' => 'Bad request',
                        ],
                        'error' => [
                            "message" =>  "Cannot earn point"
                        ]
                    ]);

        //call method
        $result = $user->postEarnPointAction();

        //check result
        $this->assertInternalType('array', $result);
        $this->assertEquals($result['status']['code'], 400);
        $this->assertEquals($result['status']['message'], 'Bad request');
        $this->assertArrayHasKey('error', $result);
        $this->assertArrayHasKey('message', $result['error']);
        $this->assertEquals($result['error']['message'], 'Cannot earn point');

    }

    public function testPostEarnPointActionSuccess()
    {
        $cacheService = Mockery::mock('CacheService');
        $cacheService->shouldReceive('deleteCacheByPrefix')->andReturn(null);

        //cache
        $this->di->set('cacheService', $cacheService, true);

         //create class
        $earnpointRepo = Mockery::mock('TruepointRepository');
        $earnpointRepo->shouldReceive("earnPoint")->andReturn([
            'success' => true,
            'message' => '',
            'data'  => [
                "customer_id"  => "7",
                "content_type" => "couponw",
                "content_id"   => "1",
                "point"        => 50,
                "status"       => "success",
                "type"         => "burn",
                "used_date"    => "2017-04-26 09:40:59",
                "id"           => "590008bb1d2dc901014049c9"
            ]
        ]);
        //create class
        $user = $this->getMockBuilder('App\Controllers\TruepointController')
                           ->setMethods([
                                'getPostInput',
                                'validateApi',
                                'getTruepointRepository',
                                'output'
                            ])
                           ->getMock();

        $user->method('getPostInput')
                   ->willReturn([]);

         $user->method('validateApi')
                   ->willReturn(['']);

        $user->method('getTruepointRepository')
                   ->willReturn($earnpointRepo);


        $user->method('output')
                   ->willReturn([
                        'status' => [
                            'code'    => 200,
                            'message' => 'Success',
                        ],
                        'data'    => [
                            "customer_id"  => "7",
                            "content_type" => "couponw",
                            "content_id"   => "1",
                            "point"        => 50,
                            "status"       => "success",
                            "type"         => "burn",
                            "used_date"    => "2017-04-26 09:40:59",
                            "id"           => "590008bb1d2dc901014049c9"
                        ]
                    ]);

        //call method
        $result = $user->postEarnPointAction();

        //check result
        $this->assertInternalType('array', $result);
        $this->assertEquals($result['status']['code'], 200);
        $this->assertEquals($result['status']['message'], 'Success');
        $this->assertArrayHasKey('data', $result);

    }


    public function testSearchHistoryActionError()
    {
        //create mock repo
        $historyRepo = Mockery::mock('TruepointRepository');
        $historyRepo->shouldReceive("getHistory")->andReturn([
            'success' => false,
            'message' => 'missinFail',
        ]);
        //create class
        $user = $this->getMockBuilder('App\Controllers\TruepointController')
                           ->setMethods([
                                'getAllUrlParam',
                                'getTruepointRepository',
                                'getDataFromCache',
                                'validateBussinessError'
                            ])
                           ->getMock();

        $user->method('getAllUrlParam')
                   ->willReturn([]);

         $user->method('getDataFromCache')
                   ->willReturn([]);

        $user->method('getTruepointRepository')
                   ->willReturn($historyRepo);

        $user->method('validateBussinessError')
                   ->willReturn([
                        'status' => [
                            'code'    => 400,
                            'message' => 'Bad request',
                        ],
                        'error' => [
                            "message" =>  "Cannot burn point"
                        ]
                    ]);

        //call method
        $result = $user->getSearchHistoryAction();

        //var_dump($result); exit;
        //check result
        $this->assertInternalType('array', $result);
        $this->assertEquals($result['status']['code'], 400);
        $this->assertEquals($result['status']['message'], 'Bad request');
        $this->assertArrayHasKey('error', $result);
        $this->assertArrayHasKey('message', $result['error']);
        $this->assertEquals($result['error']['message'], 'Cannot burn point');
    }

     public function testSearchHistoryActionNotEmptyCache()
    {

        //create class
        $deal = $this->getMockBuilder('App\Controllers\TruepointController')
                           ->setMethods([
                                'getAllUrlParam',
                                'getDataFromCache',
                                'output'
                                ])
                           ->getMock();

        $data = [
                'status' => [
                    'code'    => 200,
                    'message' => 'Success',
                ],
                "data" => [
                    [
                        'content_type' => 'deal',
                        'content_id'   => '58e267288d6a71405dbbdb30',
                        'qty'          => 2,
                    ]
                ]
            ];

        $deal->method('getAllUrlParam')
                   ->willReturn([]);

        $deal->method('getDataFromCache')
                   ->willReturn($data);

        
        $deal->method('output')
                   ->willReturn($data);

        $result = $deal->getSearchHistoryAction();

        //check result
        $this->assertInternalType('array', $result);
        $this->assertEquals($result['status']['code'], 200);
        $this->assertEquals($result['status']['message'], 'Success');
        $this->assertArrayHasKey('data', $result);
        $this->assertInternalType('array', $result['data']);
        $this->assertCount(1, $result['data']);
        $this->assertArrayHasKey('qty', $result['data'][0]);
        $this->assertEquals(2, $result['data'][0]['qty']);

    }

     public function testSearchHistoryActionSuccess()
    {

        //create mock repo
        $historyRepo = Mockery::mock('TruepointRepository');
        $historyRepo->shouldReceive("getHistory")->andReturn([
            'success' => true,
            'message' => '',
            'data'    => '58abd2f22f8331000a3acb92',
        ]);
        //create class
        $user = $this->getMockBuilder('App\Controllers\TruepointController')
                           ->setMethods([
                                'getAllUrlParam',
                                'getTruepointRepository',
                                'getDataFromCache',
                                'output'
                            ])
                           ->getMock();

        $user->method('getAllUrlParam')
                   ->willReturn([]);

        $user->method('getDataFromCache')
                   ->willReturn([]);

        $user->method('getTruepointRepository')
                   ->willReturn($historyRepo);

        $user->method('output')
                   ->willReturn([
                        'status' => [
                            'code'    => 200,
                            'message' => 'Success',
                        ],
                        'data'    => '58abd2f22f8331000a3acb92'
                    ]);

        //call method
        $result = $user->getSearchHistoryAction();

        //check result
        $this->assertInternalType('array', $result);
        $this->assertEquals($result['status']['code'], 200);
        $this->assertEquals($result['status']['message'], 'Success');
        $this->assertArrayHasKey('data', $result);
        $this->assertInternalType('string', $result['data']);
        $this->assertEquals('58abd2f22f8331000a3acb92', $result['data']);
    }

    public function testSearchHistoryActionSuccessWithLimit()
    {
        //create mock repo
        $historyRepo = Mockery::mock('TruepointRepository');
        $historyRepo->shouldReceive("getHistory")->andReturn([
            'success' => true,
            'message'     => '',
            'data'        => "58eb5ab69aaf84001429e402,58eb5bc89aaf840010437512,58ec4e299aaf840010437514",
            'totalRecord' => 6
        ]);
        //create class
        $user = $this->getMockBuilder('App\Controllers\TruepointController')
                           ->setMethods([
                                'getAllUrlParam',
                                'getTruepointRepository',
                                 'getDataFromCache',
                                'output'
                            ])
                           ->getMock();

        $user->method('getAllUrlParam')
                   ->willReturn(['limit' => 3, 'offset' => 0]);

        $user->method('getTruepointRepository')
                   ->willReturn($historyRepo);

        $user->method('getDataFromCache')
                   ->willReturn([]);

        $user->method('output')
                   ->willReturn([
                        'status' => [
                            'code'    => 200,
                            'message' => 'Success',
                        ],
                        'data'  => "58eb5ab69aaf84001429e402,58eb5bc89aaf840010437512,58ec4e299aaf840010437514",
                        'total' => [
                            'limit'       => 3,
                            'offset'      => 0,
                            'totalRecord' => 6
                        ]
                    ]);

        //call method
        $result = $user->getSearchHistoryAction();

        //check result
        $this->assertInternalType('array', $result);
        $this->assertEquals($result['status']['code'], 200);
        $this->assertEquals($result['status']['message'], 'Success');
        $this->assertArrayHasKey('data', $result);
        $this->assertInternalType('string', $result['data']);
        $this->assertEquals('58eb5ab69aaf84001429e402,58eb5bc89aaf840010437512,58ec4e299aaf840010437514', $result['data']);
        $this->assertArrayHasKey('total', $result);
        $this->assertInternalType('array', $result['total']);
        $this->assertArrayHasKey('limit', $result['total']);
        $this->assertArrayHasKey('offset', $result['total']);
        $this->assertArrayHasKey('totalRecord', $result['total']);
    }


     public function testGetHistoryDetailActionValidateError()
    {
        //create class
        $user = $this->getMockBuilder('App\Controllers\TruepointController')
                           ->setMethods([
                                'getAllUrlParam',
                                'validateApi',
                                'getDataFromCache',
                                'validateError'])
                           ->getMock();

        $user->method('getAllUrlParam')
                   ->willReturn([]);

        $user->method('getDataFromCache')
                   ->willReturn([]);

        $user->method('validateApi')
                   ->willReturn([
                        'msgError'   => 'The id is required',
                        'fieldError' => 'id'
                    ]);

        $user->method('validateError')
                   ->willReturn([
                        'status' => [
                            'code'    => 400,
                            'message' => 'Bad Request',
                        ],
                        'error' => [
                            'message'  => 'The id is required',
                            "property" => "id"
                        ]
                    ]);

        //call method
        $result = $user->getHistoryDetailAction();

        //check result
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('status', $result);
        $this->assertInternalType('array', $result['status']);
        $this->assertArrayHasKey('code', $result['status']);
        $this->assertArrayHasKey('message', $result['status']);
        $this->assertEquals($result['status']['code'], 400);
        $this->assertEquals($result['status']['message'], 'Bad Request');
        $this->assertArrayHasKey('error', $result);
        $this->assertInternalType('array', $result['error']);
        $this->assertArrayHasKey('message', $result['error']);
        $this->assertEquals($result['error']['message'], 'The id is required');
        $this->assertArrayHasKey('property', $result['error']);
        $this->assertEquals($result['error']['property'], 'id');
    }

        public function testGetLimitActionNotEmptyCache()
    {

        //create class
        $deal = $this->getMockBuilder('App\Controllers\TruepointController')
                           ->setMethods([
                                'getAllUrlParam',
                                'getDataFromCache',
                                'output'
                                ])
                           ->getMock();

        $data = [
                'status' => [
                    'code'    => 200,
                    'message' => 'Success',
                ],
                "data" => [
                    [
                        'content_type' => 'deal',
                        'content_id'   => '58e267288d6a71405dbbdb30',
                        'qty'          => 2,
                    ]
                ]
            ];

        $deal->method('getAllUrlParam')
                   ->willReturn([]);

        $deal->method('getDataFromCache')
                   ->willReturn($data);

        
        $deal->method('output')
                   ->willReturn($data);

        $result = $deal->getHistoryDetailAction();

        //check result
        $this->assertInternalType('array', $result);
        $this->assertEquals($result['status']['code'], 200);
        $this->assertEquals($result['status']['message'], 'Success');
        $this->assertArrayHasKey('data', $result);
        $this->assertInternalType('array', $result['data']);
        $this->assertCount(1, $result['data']);
        $this->assertArrayHasKey('qty', $result['data'][0]);
        $this->assertEquals(2, $result['data'][0]['qty']);

    }

    public function testGetHistoryDetailActionGetDataError()
    {
        //create mock repo
        $historyRepo = Mockery::mock('TruepointRepository');
        $historyRepo->shouldReceive("getHistoryDetail")->andReturn([
            'success' => false,
            'message' => 'missionFail'
        ]);

        //create class
        $user = $this->getMockBuilder('App\Controllers\TruepointController')
                           ->setMethods([
                                'getAllUrlParam',
                                'validateApi',
                                'getDataFromCache',
                                'getTruepointRepository',
                                'validateBussinessError'])
                           ->getMock();

        $user->method('getAllUrlParam')
                   ->willReturn($this->getDetailInputs);

        $user->method('validateApi')
                   ->willReturn($this->getDetailInputs);

        $user->method('getDataFromCache')
                   ->willReturn([]);

        $user->method('getTruepointRepository')
                   ->willReturn($historyRepo);

        $user->method('validateBussinessError')
                   ->willReturn([
                        'status' => [
                            'code'    => 400,
                            'message' => 'Bad Request',
                        ],
                        'error' => [
                            'message'  => 'Mission Fail'
                        ]
                    ]);

        //call method
        $result = $user->getHistoryDetailAction();

        //check result
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('status', $result);
        $this->assertInternalType('array', $result['status']);
        $this->assertArrayHasKey('code', $result['status']);
        $this->assertArrayHasKey('message', $result['status']);
        $this->assertEquals($result['status']['code'], 400);
        $this->assertEquals($result['status']['message'], 'Bad Request');
        $this->assertArrayHasKey('error', $result);
        $this->assertInternalType('array', $result['error']);
        $this->assertArrayHasKey('message', $result['error']);
        $this->assertEquals($result['error']['message'], 'Mission Fail');
    }

    public function testGetHistoryDetailActionGetDataSuccess()
    {
        //create mock repo
        $historyRepo = Mockery::mock('TruepointRepository');
        $historyRepo->shouldReceive("getHistoryDetail")->andReturn([
            'success' => true,
            'message' => '',
            'data'    => [
                [
                    "customer_id"  => "7",
                    "content_type" => "couponw",
                    "content_id"   => "1",
                    "point"        => 50,
                    "status"       => "success",
                    "type"         => "burn",
                    "used_date"    => "2017-04-26 09:40:59",
                    "id"           => "590008bb1d2dc901014049c9"
                ],
                [
                    "customer_id"  => "7",
                    "content_type" => "couponw",
                    "content_id"   => "1",
                    "point"        => 50,
                    "status"       => "success",
                    "type"         => "burn",
                    "used_date"    => "2017-04-26 09:40:59",
                    "id"           => "590008bb1d2dc901014049c9"
                ]
            ]
        ]);

        //create class
        $user = $this->getMockBuilder('App\Controllers\TruepointController')
                           ->setMethods([
                                'getAllUrlParam',
                                'validateApi',
                                'getTruepointRepository',
                                'getDataFromCache',
                                'output'])
                           ->getMock();

        $user->method('getAllUrlParam')
                   ->willReturn($this->getDetailInputs);

        $user->method('validateApi')
                   ->willReturn($this->getDetailInputs);

        $user->method('getDataFromCache')
                   ->willReturn([]);

        $user->method('getTruepointRepository')
                   ->willReturn($historyRepo);

        $user->method('output')
                   ->willReturn([
                        'status' => [
                            'code'    => 200,
                            'message' => 'Success',
                        ],
                        'data' => [
                            [
                                "customer_id"  => "7",
                                "content_type" => "couponw",
                                "content_id"   => "1",
                                "point"        => 50,
                                "status"       => "success",
                                "type"         => "burn",
                                "used_date"    => "2017-04-26 09:40:59",
                                "id"           => "590008bb1d2dc901014049c9"
                            ],
                            [
                                "customer_id"  => "7",
                                "content_type" => "couponw",
                                "content_id"   => "1",
                                "point"        => 50,
                                "status"       => "success",
                                "type"         => "burn",
                                "used_date"    => "2017-04-26 09:40:59",
                                "id"           => "590008bb1d2dc901014049c9"
                            ]
                        ]
                    ]);

        //call method
        $result = $user->getHistoryDetailAction();

        //check result
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('status', $result);
        $this->assertInternalType('array', $result['status']);
        $this->assertArrayHasKey('code', $result['status']);
        $this->assertArrayHasKey('message', $result['status']);
        $this->assertEquals($result['status']['code'], 200);
        $this->assertEquals($result['status']['message'], 'Success');
        $this->assertArrayHasKey('data', $result);
        $this->assertInternalType('array', $result['data']);
        $this->assertCount(2, $result['data']);
    }






    //------- end: Test function ---------//
}
