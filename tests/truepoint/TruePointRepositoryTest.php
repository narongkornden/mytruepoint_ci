<?php

use Phalcon\DI;

use App\Repositories\TruepointRepository;

class TruePointRepositoryTest extends UnitTestCase
{
    //------ start: MOCK DATA ---------//
    
    //------ end: MOCK DATA ---------//


    //------- start: Method for support test --------//
    protected static function callMethod($obj, $name, array $args) {
        $class  = new \ReflectionClass($obj);
        $method = $class->getMethod($name);
        $method->setAccessible(true);
        return $method->invokeArgs($obj, $args);
    }
    //------- end: Method for support test --------//

    //------- start: Test function --------//

    public function testGetHistoryModel()
    {
        //Mock model
        $model = Mockery::mock('Model');
        $model->shouldReceive('getModel')->andReturn("TEST");

        //register model
        $this->di->set('model', $model, true);

        //create class
        $repo = new TruepointRepository();

        //call method
        $result = $this->callMethod(
            $repo,
            'getHistoryModel',
            []
        );

        //check result
        $this->assertEquals($result, 'TEST');
    }


    public function testinsertHistoryError()
    {
        //mock model
        $model = Mockery::mock('History');
        $model->shouldReceive("save")->andReturn(false);

        //create class
        //$repo = new TruepointRepository();

        $repo = $this->getMockBuilder('App\Repositories\TruepointRepository')
                    ->setMethods(['getHistoryModel'])
                    ->getMock();

        $repo->method('getHistoryModel')
            ->willReturn($model);

        
        

        $params = [
            [
                "customer_id"  => "7",
                "content_type" => "coupon",
                "content_id"   => "1",
                "point"        => 50
            ],
            'error',
            'burn'
        ];
        //call method
        $result = $this->callMethod(
            $repo,
            'insertHistory',
            $params
        );

        //check result
        $this->assertEquals($result, false);

    }

    public function testinsertHistorySuccess()
    {
        //mock model
        $model = Mockery::mock('History');
        $model->shouldReceive("save")->andReturn(true);
        $model->shouldReceive('getOnlyData')->andReturn($model);

        //create class
        //$repo = new TruepointRepository();

        $repo = $this->getMockBuilder('App\Repositories\TruepointRepository')
                    ->setMethods(['getHistoryModel'])
                    ->getMock();

        $repo->method('getHistoryModel')
            ->willReturn($model);

        
        

        $params = [
            [
                "customer_id"  => "7",
                "content_type" => "coupon",
                "content_id"   => "1",
                "point"        => 50
            ],
            'error',
            'burn'
        ];
        //call method
        $result = $this->callMethod(
            $repo,
            'insertHistory',
            $params
        );
        //check result
         $this->assertNotNull($result);


    }

    public function testGetDetailDataById()
    {
        //Mock user
        $model = Mockery::mock('History');

        //Mock mongoService
        $mongoService = Mockery::mock('MongoService');
        $mongoService->shouldReceive('getDetailDataById')->andReturn($model);

        //register repository
        $this->di->set('mongoService', $mongoService, true);

        //create repo
        $repo = $this->getMockBuilder('App\Repositories\TruepointRepository')
                           ->setMethods([
                                'getHistoryModel'])
                           ->getMock();
                           
        $repo->method('getHistoryModel')
                   ->willReturn($model);


        //create params
        $params = ['58e27db58d6a71405dbbdb32'];

        //call method
        $result = $this->callMethod(
            $repo,
            'getDetailDataById',
            $params
        );

        //check result
        $this->assertInternalType('object', $result);
        $this->assertNotNull($result);
    }

    public function testGetDataByParamsNoLimit()
    {
        //mock mongoService
        $mongoService = Mockery::mock('MongoService');
        $mongoService->shouldReceive('createConditionFilter')->andReturn([
            'name' => ['$regex' => '^foo']
        ]);
        $mongoService->shouldReceive('manageOrderInParams')->andReturn([[
            'name' => ['$regex' => '^foo']
        ]]);
        
        //register mongoService
        $this->di->set('mongoService', $mongoService, true);

        //Mock model
        $model = Mockery::mock('Model');
        $model->shouldReceive('find')->andReturn([
            [
                "customer_id"  => "7",
                "content_type" => "couponw",
                "content_id"   => "1",
                "point"        => 50,
                "status"       => "success",
                "type"         => "burn",
                "used_date"    => "2017-04-26 09:40:59",
                "id"           => "590008bb1d2dc901014049c9"
            ]
        ]);


        //create repo
        $repo = $this->getMockBuilder('App\Repositories\TruepointRepository')
                           ->setMethods([
                                'getHistoryModel'])
                           ->getMock();

        $repo->method('getHistoryModel')
                   ->willReturn($model);

        //create params
        $params = [['name' => 'Tes%'], 'en'];

        //call method
        $result = $this->callMethod(
            $repo,
            'getDataByParams',
            $params
        );

        //check result
        $this->assertInternalType('array', $result);
        $this->assertInternalType('array', $result[0]);
        $this->assertInternalType('integer', $result[1]);
        $this->assertEquals(1, $result[1]);
        $this->assertEquals('590008bb1d2dc901014049c9', $result[0][0]['id']);

    }

    public function testGetDataByParamsHaveLimit()
    {
        //mock mongoService
        $mongoService = Mockery::mock('MongoService');
        $mongoService->shouldReceive('createConditionFilter')->andReturn([
            'username' => ['$regex' => '^foo']
        ]);
        $mongoService->shouldReceive('manageLimitOffsetInParams')->andReturn([
            'username' => ['$regex' => '^foo'], 'limit' => 3, 'skip' => 0
        ]);
        $mongoService->shouldReceive('manageOrderInParams')->andReturn([[
            'username' => ['$regex' => '^foo'], 'limit' => 3, 'skip' => 0
        ]]);

        //register mongoService
        $this->di->set('mongoService', $mongoService, true);

        //Mock model
        $model = Mockery::mock('Model');
        $model->shouldReceive('find')->andReturn([
            [
                "customer_id"  => "7",
                "content_type" => "coupon",
                "content_id"   => "1",
                "point"        => 50,
                "status"       => "success",
                "type"         => "burn",
                "used_date"    => "2017-04-26 09:40:59",
                "id"           => "590008bb1d2dc901014049c9"
            ]
        ]);

        $model->shouldReceive('count')->andReturn(1);


        //create repo
        $repo = $this->getMockBuilder('App\Repositories\TruepointRepository')
                           ->setMethods([
                                'getHistoryModel'])
                           ->getMock();
                           
        $repo->method('getHistoryModel')
                   ->willReturn($model);

        //create params
        $params = [['content_type' => 'cou%', 'limit' => 3]];

        //call method
        $result = $this->callMethod(
            $repo,
            'getDataByParams',
            $params
        );

        //check result
        $this->assertInternalType('array', $result);
        $this->assertInternalType('array', $result[0]);
        $this->assertInternalType('integer', $result[1]);
        $this->assertEquals(1, $result[1]);
        $this->assertEquals('590008bb1d2dc901014049c9', $result[0][0]['id']);
    }


    public function testGetHistoryException()
    {
        //create class
        $repo = new TruepointRepository();

        //call method 
        $result = $repo->getHistory([]);

        //check result
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('success', $result);
        $this->assertArrayHasKey('message', $result);
        $this->assertFalse($result['success']);
        $this->assertEquals('missionFail', $result['message']);

    }


    public function testGetHistorySuccessNoLimit()
    {
        //Mock type
        $model = Mockery::mock('History');

        //Mock mongo services
        $mongoService = Mockery::mock('MongoService');
        $mongoService->shouldReceive('getAllIdFromDatas')->andReturn('58eb5ab69aaf84001429e402');

        //register model
        $this->di->set('mongoService', $mongoService, true);
        //creste class
        $repo = $this->getMockBuilder('App\Repositories\TruepointRepository')
                    ->setMethods(['getDataByParams'])
                    ->getMock();

        $repo->method('getDataByParams')
            ->willReturn([$model, 1]);

        //call method 
        $result = $repo->getHistory([]);

        //check result
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('success', $result);
        $this->assertArrayHasKey('message', $result);
        $this->assertTrue($result['success']);
        $this->assertArrayHasKey('data', $result);
        $this->assertInternalType('string', $result['data']);
        $this->assertEquals('58eb5ab69aaf84001429e402', $result['data']);

    }

    public function testGetHistorySuccessHaveLimit()
    {
        //Mock type
        $model = Mockery::mock('History');

        //Mock mongo services
        $mongoService = Mockery::mock('MongoService');
        $mongoService->shouldReceive('getAllIdFromDatas')->andReturn('58eb5ab69aaf84001429e402');

        //register model
        $this->di->set('mongoService', $mongoService, true);
        //creste class
        $repo = $this->getMockBuilder('App\Repositories\TruepointRepository')
                    ->setMethods(['getDataByParams'])
                    ->getMock();

        $repo->method('getDataByParams')
            ->willReturn([$model, 1]);

        //call method 
        $result = $repo->getHistory(['limit' => 2]);

        //check result
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('success', $result);
        $this->assertArrayHasKey('message', $result);
        $this->assertTrue($result['success']);
        $this->assertArrayHasKey('data', $result);
        $this->assertInternalType('string', $result['data']);
        $this->assertEquals('58eb5ab69aaf84001429e402', $result['data']);
        $this->assertArrayHasKey('totalRecord', $result);
        $this->assertEquals(1, $result['totalRecord']);

    }

    public function testGetHistoryDetailException()
    {
        //create class
        $repo = new TruepointRepository();

        //call method 
        $result = $repo->getHistoryDetail([]);

        //check result
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('success', $result);
        $this->assertArrayHasKey('message', $result);
        $this->assertFalse($result['success']);
        $this->assertEquals('missionFail', $result['message']);

    }

    public function testGetHistoryDetail()
    {
        //Mocke category
        $model = Mockery::mock('History');

   

        //Mock mongo services
        $mongoService = Mockery::mock('MongoService');

        //register model
        $this->di->set('mongoService', $mongoService, true);

        $mongoService->shouldReceive('manageSortDataByIdList')->andReturn([[
            "customer_id"  => "7",
            "content_type" => "couponw",
            "content_id"   => "1",
            "point"        => 50,
            "status"       => "success",
            "type"         => "burn",
            "used_date"    => "2017-04-26 09:40:59",
            "id"           => "590008bb1d2dc901014049c9"
        ]]);

       

        //create class
        $repo = $this->getMockBuilder('App\Repositories\TruepointRepository')
                           ->setMethods([
                                'getHistoryModel','getDetailDataByIdLargeData'
                            ])
                           ->getMock();
        $repo->method('getDetailDataByIdLargeData')
                   ->willReturn($model);
                           
        $repo->method('getDetailDataByIdLargeData')
                   ->willReturn($model);

        //call method 
        $result = $repo->getHistoryDetail(['id' => '58eb5ab69aaf84001429e402']);

        //check result
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('success', $result);
        $this->assertArrayHasKey('message', $result);
        $this->assertTrue($result['success']);
        $this->assertArrayHasKey('data', $result);
        $this->assertInternalType('array', $result['data']);
        $this->assertArrayHasKey('id', $result['data'][0]);
        $this->assertArrayHasKey('content_type', $result['data'][0]);
        $this->assertArrayHasKey('content_id', $result['data'][0]);

    }

     public function testBurnPointError()
    {
        //mock model
        $model = Mockery::mock('History');

        //creste class
        $repo = $this->getMockBuilder('App\Repositories\TruepointRepository')
                    ->setMethods(['getHistoryModel','mockredeemPoint', 'insertHistory'])
                    ->getMock();

        $repo->method('getHistoryModel')
            ->willReturn($model);

         $repo->method('mockredeemPoint')
            ->willReturn([
                'status' => [
                'code' => 400
                    ], 
                    'error' => [
                        'message' => 'no point to earn'
                    ]
                ]);

        $repo->method('insertHistory')
            ->willReturn(null);

        $params = [
            "customer_id"  => "7",
            "content_type" => "couponw",
            "content_id"   => "1",
            "point"        => 50,
        ];
        //call method
        $result = $repo->burnPoint($params);

        //check result
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('success', $result);
        $this->assertFalse($result['success']);
        $this->assertArrayHasKey('message', $result);
        $this->assertEquals('insertError', $result['message']);
    }

     public function testBurnPointSuccess()
    {
        //mock user
        $history  = Mockery::mock('History');
        //mock model
        $model = Mockery::mock('Model');

        //Mock mongo services
        $mongoService = Mockery::mock('MongoService');
        $mongoService->shouldReceive('addIdTodata')->andReturn(
                [
                    "customer_id"  => "7",
                    "content_type" => "couponw",
                    "content_id"   => "1",
                    "point"        => 50,
                    "status"       => "success",
                    "type"         => "burn",
                    "used_date"    => "2017-04-26 09:40:59",
                    "id"           => "590008bb1d2dc901014049c9"
                ]
            );

        //register model
        $this->di->set('mongoService', $mongoService, true);

        //creste class
        $repo = $this->getMockBuilder('App\Repositories\TruepointRepository')
                    ->setMethods(['getHistoryModel','insertHistory'])
                    ->getMock();

        $repo->method('getHistoryModel')
            ->willReturn($model);


        $repo->method('insertHistory')
            ->willReturn($history);

        $params = [
            "customer_id"  => "7",
            "content_type" => "couponw",
            "content_id"   => "1",
            "point"        => 50,
        ];
        //call method
        $result = $repo->burnPoint($params);

        //var_dump($result); exit;
        //check result
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('success', $result);
        $this->assertTrue($result['success']);
        $this->assertArrayHasKey('data', $result);
        $this->assertArrayHasKey('id', $result['data']);
    }


     public function testEarnPointError()
    {
        //mock model
        $model = Mockery::mock('History');

        //creste class
        $repo = $this->getMockBuilder('App\Repositories\TruepointRepository')
                    ->setMethods(['getHistoryModel','mockredeemPoint', 'insertHistory'])
                    ->getMock();

        $repo->method('getHistoryModel')
            ->willReturn($model);

         $repo->method('mockredeemPoint')
            ->willReturn([
                'status' => [
                'code' => 400
                    ], 
                    'error' => [
                        'message' => 'no point to earn'
                    ]
                ]);

        $repo->method('insertHistory')
            ->willReturn(null);

        $params = [
            "customer_id"  => "7",
            "content_type" => "couponw",
            "content_id"   => "1",
            "point"        => 50,
        ];
        //call method
        $result = $repo->earnPoint($params);

        //check result
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('success', $result);
        $this->assertFalse($result['success']);
        $this->assertArrayHasKey('message', $result);
        $this->assertEquals('insertError', $result['message']);
    }

    public function testEarnPointSuccess()
    {
        //mock user
        $history  = Mockery::mock('History');
        //mock model
        $model = Mockery::mock('Model');

        //Mock mongo services
        $mongoService = Mockery::mock('MongoService');
        $mongoService->shouldReceive('addIdTodata')->andReturn(
                [
                    "customer_id"  => "7",
                    "content_type" => "couponw",
                    "content_id"   => "1",
                    "point"        => 50,
                    "status"       => "success",
                    "type"         => "earn",
                    "used_date"    => "2017-04-26 09:40:59",
                    "id"           => "590008bb1d2dc901014049c9"
                ]
            );

        //register model
        $this->di->set('mongoService', $mongoService, true);

        //creste class
        $repo = $this->getMockBuilder('App\Repositories\TruepointRepository')
                    ->setMethods(['getHistoryModel','insertHistory'])
                    ->getMock();

        $repo->method('getHistoryModel')
            ->willReturn($model);


        $repo->method('insertHistory')
            ->willReturn($history);

        $params = [
            "customer_id"  => "7",
            "content_type" => "couponw",
            "content_id"   => "1",
            "point"        => 50,
        ];
        //call method
        $result = $repo->earnPoint($params);

        //check result
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('success', $result);
        $this->assertTrue($result['success']);
        $this->assertArrayHasKey('data', $result);
        $this->assertArrayHasKey('id', $result['data']);
    }


    public function testBurnPointRetryError(){

        //mock model
        $model = Mockery::mock('History');

        //creste class
        $repo = $this->getMockBuilder('App\Repositories\TruepointRepository')
                    ->setMethods(['getHistoryModel','mockredeemPoint', 'updateStatus'])
                    ->getMock();

        $repo->method('getHistoryModel')
            ->willReturn($model);

         $repo->method('mockredeemPoint')
            ->willReturn([
                'status' => [
                'code' => 400
                    ], 
                    'error' => [
                        'message' => 'no point to earn'
                    ]
                ]);

        $repo->method('updateStatus')
            ->willReturn(false);

        $params = [
            "id"           => "59004d1b62c260000f228a45",
            "customer_id"  => "7",
            "content_type" => "couponw",
            "content_id"   => "1",
            "point"        => 50,
        ];
        //call method
        $result = $repo->burnPointRetry($params);

        //check result
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('success', $result);
        $this->assertFalse($result['success']);
        $this->assertArrayHasKey('message', $result);
        $this->assertEquals('updateError', $result['message']);

    }

    public function testBurnPointRetrySuccess(){

        //mock model
        $model = Mockery::mock('History');

        $mongoService = Mockery::mock('MongoService');
        $mongoService->shouldReceive('addIdTodata')->andReturn(
                [
                    "customer_id"  => "7",
                    "content_type" => "couponw",
                    "content_id"   => "1",
                    "point"        => 50,
                    "status"       => "success",
                    "type"         => "burn",
                    "used_date"    => "2017-04-26 09:40:59",
                    "id"           => "590008bb1d2dc901014049c9"
                ]
            );

        //register model
        $this->di->set('mongoService', $mongoService, true);


        //creste class
        $repo = $this->getMockBuilder('App\Repositories\TruepointRepository')
                    ->setMethods(['getHistoryModel','mockredeemPoint', 'updateStatus'])
                    ->getMock();

        $repo->method('getHistoryModel')
            ->willReturn($model);

         $repo->method('mockredeemPoint')
            ->willReturn([
                'status' => [
                'code' => 200
                    ], 
                    'error' => [
                        'message' => 'no point to earn'
                    ]
                ]);

        $repo->method('updateStatus')
            ->willReturn($model);

        $params = [
            "id"           => "590008bb1d2dc901014049c9",
            "customer_id"  => "7",
            "content_type" => "couponw",
            "content_id"   => "1",
            "point"        => 50,
        ];
        //call method
        $result = $repo->burnPointRetry($params);


        //check result
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('success', $result);
        $this->assertTrue($result['success']);
        $this->assertArrayHasKey('data', $result);
        $this->assertArrayHasKey('id', $result['data']);

    }

    public function testGetListBurnretry(){
        //Mock type
        $model = Mockery::mock('History');

        //Mock mongo services
        $mongoService = Mockery::mock('MongoService');
        $mongoService->shouldReceive('getAllIdFromDatas')->andReturn(
                [
                    "customer_id"  => "7",
                    "content_type" => "couponw",
                    "content_id"   => "1",
                    "point"        => 50,
                    "status"       => "success",
                    "type"         => "burn",
                    "used_date"    => "2017-04-26 09:40:59",
                    "id"           => "590008bb1d2dc901014049c9"
                ]
            );

        //register model
        $this->di->set('mongoService', $mongoService, true);
        //creste class
        $repo = $this->getMockBuilder('App\Repositories\TruepointRepository')
                    ->setMethods(['getDataByParams'])
                    ->getMock();

        $repo->method('getDataByParams')
            ->willReturn([$model, 1]);


        //call method 
        $result = $repo->getListBurnretry([]);

        //check result
        $this->assertInternalType('array', $result);
        $this->assertEquals('590008bb1d2dc901014049c9', $result['id']);
    }


        public function testGetDataById()
    {
        //Mock model
        $model = Mockery::mock('Message');
        $model->shouldReceive('findById')->andReturn('Test');
        
        //create class
        $repo = $this->getMockBuilder('App\Repositories\TruepointRepository')
                           ->setMethods([
                                'getHistoryModel'])
                           ->getMock();
                           
        $repo->method('getHistoryModel')
                   ->willReturn($model);


        $result = $this->callMethod(
            $repo,
            'getDataById',
            ['1']
        );

        $this->assertEquals('Test', $result);
    }


    public function testUpdateStatusError()
    {
        //mock model
        $model = Mockery::mock('History');
        $model->shouldReceive("save")->andReturn(false);
        $model->retry = 1;


        $repo = $this->getMockBuilder('App\Repositories\TruepointRepository')
                    ->setMethods(['getDataById'])
                    ->getMock();

        $repo->method('getDataById')
            ->willReturn($model);

    
        $params = ['59004d1b62c260000f228a45','success'];
        //call method
        $result = $this->callMethod(
            $repo,
            'updateStatus',
            $params
        );

        //check result
        $this->assertEquals($result, false);

    }

     public function testUpdateStatusSuccess()
    {
        //mock model
        $model = Mockery::mock('History');
        $model->shouldReceive("save")->andReturn(true);
        $model->shouldReceive('getOnlyData')->andReturn($model);
        $model->retry = 1;


        $repo = $this->getMockBuilder('App\Repositories\TruepointRepository')
                    ->setMethods(['getDataById'])
                    ->getMock();

        $repo->method('getDataById')
            ->willReturn($model);

    
        $params = ['59004d1b62c260000f228a45','success'];
        //call method
        $result = $this->callMethod(
            $repo,
            'updateStatus',
            $params
        );

        //check result
        $this->assertEquals($result, $model);

    }


    

    
    //------- end: Test function --------//
}