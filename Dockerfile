FROM akkapong/php-apache-phalcon

WORKDIR /var/www/html
COPY . ./

RUN composer install

# ## Add apache config
ADD docker/web/sites-enabled/vhost.conf /etc/apache2/sites-enabled/

#ADD docker/web/key.pem /data


